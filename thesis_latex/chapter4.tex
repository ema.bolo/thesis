\chapter{Fuel Optimal Landing Problem}
\label{chapter4}
\thispagestyle{empty}

% \begin{quotation}
% 	{\footnotesize
% 	\noindent \emph{``Bud: No, calma, calma, stiamo calmi, noi siamo su un'isola deserta, e per il momento non t'ammazzo perch\'e mi potresti servire come cibo ...''}
% 	\begin{flushright}
% 	Chi trova un amico trova un tesoro
% 	\end{flushright}
% 	}
% \end{quotation}
% \vspace{0.5cm}

\noindent While the EO problem was mostly used as a case study to test and understand DAgger, the FO problem is more interesting for practical application, since it represents an optimal landing where the fuel consumption is minimized. Furthermore, the module of the control thrust is constrained between a minimum value $T_{min}$ and a maximum value $T_{max}$ as it would be in a real situation.\\

\noindent Consider a lander in a 3D Cartesian reference system with axes $\{ \bm{\hat{x}}, \bm{\hat{y}}, \bm{\hat{z}} \}$, subject to the gravity force $\bm{g}$ and control thrust $\bm{T}$. It starts at time $t_0$ from initial position $\bm{r}_0$ with initial velocity $\bm{v}_0$, and it has to reach the target state $\bm{r}_f$, $\bm{v}_f$ at time $t_f$. The equations of motion are:
\begin{equation}
    \begin{displaystyle}
        \begin{cases}
            \setlength{\jot}{4pt}
            \begin{aligned}
                \dot{\bm{r}} &= \bm{v}\\
                \dot{\bm{v}} &= \frac{\bm{T}}{m} + \bm{g}\\
                \dot{m} &= -\frac{||\bm{T}||}{I_{sp} \cdot g0}\\
            \end{aligned}
        \end{cases} ~ BC: ~
        \begin{cases}
            \begin{aligned}
                \bm{r}(t_0) &= \bm{r}_0\\
                \bm{v}(t_0) &= \bm{v}_0\\
                m(t_0) &= m_0\\
            \end{aligned}
        \end{cases}~
        \begin{cases}
            \begin{aligned}
                \bm{r}(t_f) &= \bm{r}_f\\
                \bm{v}(t_f) &= \bm{v}_f\\
            \end{aligned}
        \end{cases}
    \end{displaystyle}
    \label{eq:EoM_FO}
\end{equation}

\noindent where $g = 1.62 \ N/kg$ is the gravitational field of the Moon, $g0 = 9.81 \ N/kg$ the gravitational field of the Earth and $I_{sp} = 200 \ s$ the specific impulse of the lander's engine. The cost function is:

\begin{equation} J_{FO} = \text{min} \int_{t_0}^{t_f} ||\bm{T}|| \ dt \end{equation}


\section{GPOPS}
\label{sec:gpops}
GPOPS, which stands for \textit{General Pseudospectral Optimal Control
Software}, is the software used to generate optimal trajectories for the FO problem. It uses a direct method for solving the two-points-boundary-value-problem (TPBVP), and for this reason the solution is not very accurate, compared to a software using an indirect method. Anyway, it is accurate enough to generate a dataset to train a ML model. GPOPS has in particular one parameter called \textit{mesh tolerance} which influences the accuracy of the solution. Fig. \ref{fig:gpops_solutions_compared} shows two examples of GPOPS solutions for a 1D trajectory, one with a mesh tolerance of $10^{-4}$ and one with a mesh tolerance of $10^{-8}$. It is clear that a smaller mesh tolerance gives a more accurate solution (in particular for the output) and also more points. 

It is known from optimal control theory for a fuel optimal landing problem that the thrust profile is bang-bang. For this reason, the output is categorical: 0 means that the thrust is minimum ($1000 \ N$), 1 means that it is maximum ($3400 \ N$). The thrust is then computed as $T = 1000 + 2400 \cdot T_c \ [N]$. As Fig. \ref{fig:gpops_solutions_compared} (a) and (b) show, in the output solution there is in both cases a middle point during the switching time: that is a collateral effect of the direct method used to solve the TPBVP. Different parameters, other than the mesh tolerance, have been tweaked in order to try to remove it, but without success. As a solution, the middle points are just removed from the train set.

\begin{figure}[H]
    \centering
    \subfigure[Mesh tolerance $10^{-4}$]
    {\includegraphics[width=0.49\linewidth]{img/gpops_solution_1e-4.png}}
    \subfigure[Mesh tolerance $10^{-8}$]
    {\includegraphics[width=0.49\linewidth]{img/gpops_solution_1e-8.png}}
    \caption{Examples of GPOPS solution for 1D case}
    \label{fig:gpops_solutions_compared}
\end{figure}


\section{Choice of the input features}
\noindent It is worth to explain the choice of using the mass as input feature. In fact, Izzo \cite{izzo1} used a DNN for a FO problem, and he assumed complete knowledge of position and velocity, but did not use the mass as input feature. However, the state of the dynamics includes the mass, which indeed is a variable in the equations of motions \ref{eq:EoM_FO}. The DNNs and ELMs, unlike RNNs which consider the observation's history, only take one observation at a time to predict the output: this means that it is needed to respect the Markov Property discussed in section \ref{sec:markov_property}, otherwise the model lacks information about the spacecraft's state, and may make wrong predictions because of it. For example, consider the following initial conditions for a 1D problem:

\begin{itemize}
    \item $X_1 = [450 \ m, -40 \ m/s, 1000 \ kg]$
    \item $X_2 = [450 \ m, -40 \ m/s, 950 \ kg]$
\end{itemize}

\noindent After generating a trajectory from these initial conditions with GPOPS, the first state is labeled with a maximum thrust $y_1 = 1$, while the second state is labeled with a minimum thrust $y_2 = 0$. This explains why the mass needs to be used as input feature.

Note that this matter was not present in the EO problem, because the control acceleration was not constrained and so the dynamics was ultimately not dependent on the mass, as eqs. \ref{eq:EoM_ZEM_ZEV} show.

Finally, it is pointed out that the mass variation is usually not measured on-board by the spacecrafts, but it actually could be easily calculated in real time from the initial mass and the control thrust history:
\begin{equation} m(t_k) = m(t_0) - \sum_{i=1}^k \frac{||\bm{T}_{i-1}||}{I_{sp} \cdot g0} (t_{k} - t_{k-1}) \end{equation}



\section{1D case}
% The one-dimensional problem is of interest for two main reasons: first, it is a simplified problem that allows to test the network training and the DAgger techniques fast and easily, since it requires less training data than a more complex 3D case; second, the reduced number of dimensions allows to visualize results that offer interesting insights of the ML model behavior, and that would otherwise more difficult or impossible to visualize with more dimensions.


\subsection{Problem formulation}
\label{sec:GPOPS_1D_problem_formulation}
Consider a lander subject to the gravity force and the control force. It starts at time $t_0 = 0 \ s$ from the initial altitude $h_0$ with the initial velocity $v_0$ and the initial mass $m_0$, and it has to reach the final state $h_f = 0 \ m$ and $v_f = 0 \ m/s$ at time $t_f$. 
The equations of motion are: 
\begin{equation}
    \begin{displaystyle}
        \begin{cases}
        	\setlength{\jot}{4pt}
            \begin{aligned}
				\dot{h} &= v\\
				\dot{v} &= \frac{T}{m} - g\\
				\dot{m} &= -\frac{T}{I_{sp} \cdot g0}
			\end{aligned}
        \end{cases}~ BC: ~
        \begin{cases}
        	\begin{aligned}
        		h(0) &= h_0\\
            	v(0) &= v_0\\
            	m(0) &= m_0\\
        	\end{aligned}
        \end{cases}~
        \begin{cases}
        	\begin{aligned}
	            h(t_f) &= 0\\
	            v(t_f) &= 0\\
            \end{aligned}
        \end{cases}
    \end{displaystyle}
    \label{eq:EoM1D_GPOPS}
\end{equation}

\noindent where $h$ is the altitude, $v$ the vertical velocity, $T$ the control thrust. Unlike the EO problem, now the thrust is constrained to be between 1000 N and 3400 N. As will be explained in section \ref{sec:GPOPS_1D_dagger_procedure}, this causes practical issues during the DAgger procedure that need to be solved with some trick.

The goal is to train a network able to give for each state of the trajectory a control thrust $T$, such that the lander reaches the final state minimizing the cost function:
\begin{equation} J_{FO} = \text{min} \int_0^{t_f} ||T|| dt \end{equation}

\noindent In order to train the model, supervised learning techniques are used. The altitude $h$, velocity $v$ and mass $m$ are used as input features. The target $t_c$ is categorical (0-1).

\begin{itemize}
    \item Input features: $$X = [ h, v, m ] \hspace{1cm} X \in \mathbb{R}^{3 \times 1}$$
    \item Target: $$y = t_c \hspace{1cm} y \in \mathbb{R}^{1 \times 1}$$
\end{itemize}


\subsection{Train and test dataset generation}
% PARTIAL DEPENDENCE PLOT
% 500 trajectories, 10.000 points, tolerance 1e-5
For the FO 1D landing problem, 500 optimal trajectory are generated with GPOPS, setting the mesh tolerance of GPOPS to $10^{-5}$, obtaining a total of approximtely 10.000 states. The initial conditions for these trajectories are sampled uniformly randomly within the following ranges:

\begin{table}[H]
    \centering
    \begin{tabular}{ | p{3cm} | p{3cm} | p{3cm} | }
    \hline 
    \textbf{Variable} & \textbf{Min. value} & \textbf{Max. value}\\
    \hline
    $h_0$ & 1000 m & 1500 m\\
    $v_0$ & -40 m/s & -20 m/s\\
    $m_0$ & 1200 kg & 1400 kg\\
    \hline
    \end{tabular}
    \caption{FO 1D train set initial condition}
\end{table}

\noindent The train set is visualized in Fig. \ref{fig:FO_1D_train_set}, both from a 2D and 3D perspective. The points in red represent states in which the optimal thrust is maximum, while the points in blue represent states where the optimal thrust is minimum.

\begin{figure}[H]
    \centering
    \subfigure[2d view]
    {\includegraphics[width=0.49\linewidth]{img/GPOPS_1D_DNN_train_ds_0_2d.png}}
    \subfigure[3d view]
    {\includegraphics[width=0.49\linewidth]{img/GPOPS_1D_DNN_train_ds_0_3d.png}}
    \caption{FO 1D train set}
    \label{fig:FO_1D_train_set}
\end{figure}


\noindent One may wonder why the train set is generated varying the initial condition of the mass; after all, it is reasonable to assume that the lander will have a fixed known mass when it begins the landing. Before explaining the reason of this choice, it is introduced a plot called \textit{partial dependence plot} (PDP), which is a tool used in ML to get an insight of the  model's behavior. Partial dependence plots show how each variable affects the model's predictions. This is done making one variable to vary, while fixing all other variables, and looking how the predictions of the model depend on that variable (like a partial derivative). Figure \ref{fig:partial_dependence_plot1} shows an example of PDP: there are three plots, one where the position is varied, one for the velocity and one for the mass. When they do not vary, position is fixed to $700 \ m$, velocity to $-40 \ m/s$ and mass to $1300 \ kg$. The first two plots look correct, in fact with parity of velocity and mass, it is intuitive that the thrust is maximum if the position is lower; and it is intuitive that, with parity of position and mass, the thrust is maximum if the negative velocity is higher in absolute value. What looks wrong is the fact that, with parity of position and velocity, the thrust is predicted maximum if the mass is lower.\\

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{img/PartialDependencePlot_mass_const.png}
    \caption{Partial dependence plot 1}
    \label{fig:partial_dependence_plot1}
\end{figure}

\noindent This happens because if all the training trajectories begin with the same initial mass, the model learns a wrong correlation between the mass and the output. From an intuitive point of view, this is because, if there is a limited space of initial conditions, what the model sees is just a pattern that links low mass (i.e. when the lander is in an advanced phase of the landing) to maximum thrust. If, instead, the training trajectories are generated by varying the initial mass, the PDP obtained is the one represented in Fig. \ref{fig:partial_dependence_plot2}, and now the third plot shows the behavior that is expected.\\

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{img/PartialDependencePlot_mass_varies.png}
    \caption{Partial dependence plot 2}
    \label{fig:partial_dependence_plot2}
\end{figure}


\subsection{Dynamics simulator}
The dynamics simulator works like the one implemented for the EO problem, with a few changes.

\noindent For each initial condition of the test trajectories:
\begin{enumerate}
    \item Initialize position $h = h_0$, velocity $v = v_0$ and mass $m = m_0$
    \item Apply MinMaxScaler to input features $X = [h, v, m]$, and predict thrust $y_{pred} = t_c$
    \item Compute the thrust as $T = 1000 + 2400 \cdot t_c \ [N]$; integrate equation of motion \ref{eq:EoM1D_GPOPS} with a time step of 0.1 s (10 Hz)
    \item If the altitude $h > 0 \ m$ and the velocity $v < 0 \ m/s$ go back to point 2, otherwise interrupt integration
\end{enumerate}


\subsection{DAgger procedure}
\label{sec:GPOPS_1D_dagger_procedure}
Here it is explained how the DAgger algorithm already described in section \ref{sec:dagger} is implemented for the FO one-dimensional problem:

\begin{enumerate}
    \item The train set is generated with GPOPS and the ML model is trained. 

    \item A Monte Carlo simulation is performed: the current policy is run with the test trajectories to obtain the predicted trajectories.

    \item The predicted trajectories are labeled with GPOPS. The states where the predicted thrust is wrong ($y_{pred} \neq y_{opt}$) are collected. 

    \item The train set is augmented with the states collected in the previous point. The procedure is repeated going back to point 1 and training a new model on the augmented train set.
\end{enumerate}

\noindent As anticipated in section \ref{sec:GPOPS_1D_problem_formulation}, the constraint on the thrust levels causes some problems in the DAgger procedure that was not present in the EO problem, where the control acceleration was not bounded. In particular, point 3 of the previous list is not trivial as it seems: it may happen that the predicted trajectories deviate that much from the optimal trajectories that some states are out of the reachability point for the optimal solution, and GPOPS is not able to solve the FO problem from the those states because the thrust is limited (e.g. the altitude is too low and the downward velocity is too high). A few solutions to this problem have been considered:

\begin{itemize}
    \item Increase the mesh tolerance of GPOPS (e.g. to $10^{-3}-10^{-2}$). A bigger mesh tolerance reduces the accuracy of the solution, but makes GPOPS more flexible in finding a solution; this is acceptable, since only the first state of the trajectory is of interest for DAgger.

    \item Just remove the states from where GPOPS is not able to solve the FO problem. This is feasible only for a few states, because it has to be done manually each time that GPOPS is stuck in trying to solve the FO problem. If a lot of states are out of the reachability zone, then consider the third solution.

    \item Start the first iteration correcting the predicted trajectories only until a certain point (e.g. half of each trajectory); then, gradually increase the stop point with the iterations. In fact, the trained policy is supposed to improve along the iterations and therefore it should be able to reach states more and more near to the target state without deviating too much from the optimal trajectory.
\end{itemize}

\noindent A mix of all the three solutions proposed has been applied.


\subsection{Visualize DAgger benefits}
As for the EO 1D case, it is possible to visualize how applying DAgger makes the predicted trajectories to converge better to the target state. Figure \ref{fig:predTraj_over_trainSet_FO_2d} and \ref{fig:predTraj_over_trainSet_FO_3d}, respectively from a 2D and 3D perspective, show how augmenting the train dataset helps the predicted trajectory to converge to the target state $[0 \ m, 0 \ m/s]$. 

\begin{figure}[H]
    \centering
    \subfigure[Before DAgger]
    {\includegraphics[width=0.49\linewidth]{img/predTrajOverTrainSet_0_2d.png}}
    \subfigure[After DAgger]
    {\includegraphics[width=0.49\linewidth]{img/predTrajOverTrainSet_4_2d.png}}
    \caption{Predicted trajectories over train set, before and after DAgger, 2D view}
    \label{fig:predTraj_over_trainSet_FO_2d}
\end{figure}

\begin{figure}[H]
    \centering
    \subfigure[Before DAgger]
    {\includegraphics[width=0.49\linewidth]{img/predTrajOverTrainSet_0_3d.png}}
    \subfigure[After DAgger]
    {\includegraphics[width=0.49\linewidth]{img/predTrajOverTrainSet_4_3d.png}}
    \caption{Predicted trajectories over train set, before and after DAgger, 3D view}
    \label{fig:predTraj_over_trainSet_FO_3d}
\end{figure}



\subsection{Deep Neural Network}
The original size of the 1D train set with 500 trajectories is approximately 10.000
states. Like in the other cases, the network complexity increases over the DAgger iterations, as Fig. \ref{fig:FO_1D_DNN_architecture} shows.

\begin{figure}[H]
    \centering
    \subfigure[Iteration 0]
    {\includegraphics[width=0.49\linewidth]{../1D/GPOPS/DNN/dagger/model/DNN_0.png}}
    \subfigure[Iteration 4]
    {\includegraphics[width=0.49\linewidth]{../1D/GPOPS/DNN/dagger/model/DNN_4.png}}
    \caption{FO 1D DNN architecture}
    \label{fig:FO_1D_DNN_architecture}
\end{figure}

\noindent The following hyperparameters are common to all the architecures:
\begin{itemize}
    \item Learning rate: $10^{-3}$, decreases by a factor of 0.9 each time the validation loss does not decreases for 5 consecutive epochs.
    \item Mini-batch size: 8, same used by Izzo in \cite{izzo1}.
    \item Activation function: ReLU, same used by Izzo in \cite{izzo1}.
    \item Normalization: input features are rescaled in a range $[0, 100]$ using the MinMaxScaler of Keras.
\end{itemize}

\noindent The following hyperparameters, instead, are searched again for each iteration:
\begin{itemize}
    \item Number of hidden layers.
    \item Number of neurons per layer.
\end{itemize}

\noindent An example of confusion matrix is represented in Fig. \ref{fig:FO_1d_elm_confusion_matrix}. It shows that over a total of 5000 test states:

\begin{itemize}
    \item 3547 are correctly predicted as max thrust ($100 \%$ of the states with max thrust)
    \item 1442 are correctly predicted as min thrust ($99.2 \%$ of the states with min thrust)
    \item 11 are wrongly predicted as max thrust ($0.8 \%$ of the states with min thrust)
    \item 0 are wrongly predicted as min thrust ($0 \%$ of the states with max thrust)
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{../1D/GPOPS/DNN/dagger/img/0_ConfusionMatrix.png}
    \caption{FO 1D DNN Confusion matrix}
    \label{fig:FO_1d_dnn_confusion_matrix}
\end{figure}

\noindent The performances of DNN over the DAgger iterations are reported in Table \ref{tab:FO_1d_dnn_results_dagger}, and compared to classical supervised learning in Fig. \ref{fig:FO_1d_dnn_results_dagger_vs_supervised}.

\begin{table}[H]
    \centering
    \begin{tabular}{ | p{1.5cm} | p{1.5cm} | p{1.5cm} | p{1.6cm} | p{1.5cm} | p{1.6cm} | p{1.5cm} | }
    \hline
    \rowcolor{DarkBlue}
    % \mc{1}{}  & \mc{1}{x} & \mc{1}{y} & \mc{1}{w} & \mc{1}{z} \\
    \hline
    \color{white}{Iter} & \color{white}{Val acc} & \color{white}{Pos $[m]$} & \color{white}{Vel $[m/s]$} & \color{white}{$D_{CM}$} & \color{white}{New data} & \color{white}{Data}\\
    
    0 & $0.99$ & $13.6$ & $3.91$ & $1.9 \cdot 10^{-3}$ & - & $9,117$ \\
    
    \rowcolor{Gray}
    1 & $0.99$ & $3.72$ & $1.94$ & $1.8 \cdot 10^{-3}$ & 397 & $9,514$ \\

    2 & $0.99$ & $2.60$ & $4.27$ & $1.7 \cdot 10^{-3}$ & 43 & $9,556$ \\
    
    \rowcolor{Gray}
    3 & $0.99$ & $1.54$ & $0.81$ & $1.3 \cdot 10^{-3}$ & 293 & $9,849$ \\

    4 & $0.99$ & $0.48$ & $1.02$ & $8.4 \cdot 10^{-4}$ & 905 & $10,754$ \\

    \hline
    \end{tabular}
    \caption{FO 1D DNN results DAgger}
    \label{tab:FO_1d_dnn_results_dagger}
\end{table}

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{../1D/GPOPS/DNN/results_dagger_vs_super.png}
    \caption{FO 1D DNN results DAgger vs Supervised. The subplots represent the validation loss, the average final position, the average final velocity and the Cramer-Von Mises distance.}
    \label{fig:FO_1d_dnn_results_dagger_vs_supervised}
\end{figure}


\subsection{Extreme Learning Machine}
While the ELM proved to be extremely powerful for the regression problem, it showed poor performances on the classification problem. It is not clear this kind of behavior, because in general ELMs are supposed to work well also on classification and multi-classification problems \cite{elm_paper}. Figure \ref{fig:FO_1d_elm_confusion_matrix} shows an example of classification matrix, while Fig. \ref{fig:FO_1d_elm_state_distribution} shows and example of distribution of states. Application of DAgger did not bring any improvement on the performances of ELM, for this reason ELM is not used for the FO problem where classification is required.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{../1D/GPOPS/ELM/dagger/img/ConfusionMatrix.png}
    \caption{FO 1D ELM Confusion Matrix}
    \label{fig:FO_1d_elm_confusion_matrix}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{../1D/GPOPS/ELM/dagger/img/StateDistribution.png}
    \caption{FO 1D ELM State Distribution}
    \label{fig:FO_1d_elm_state_distribution}
\end{figure}


\section{3D case}
Once DAgger has been successfully applied to the 1D case, it is now time to tackle the more complete and realistic 3D case.


\subsection{Problem formulation}
Consider a lander subject to the downward gravity force and a 3D vector control force. It starts at time $t_0 = 0$ at initial position [$x_0$, $y_0$, $z_0$] with initial velocity [${v_x}_0$, ${v_y}_0$, ${v_z}_0$] and initial mass $m_0$, and it has to reach the final state $x_f = y_f = z_f = 0 \ m$ and ${v_x}_f = {v_y}_f = {v_z}_f = 0 \ m/s$ at time $t_f$. 
The equations of motion are:
\begin{equation}
    \begin{displaystyle}
        \begin{cases}
        	\setlength{\jot}{4pt}
            \begin{aligned}
				\dot{x} &= v_x\\
				\dot{y} &= v_y\\
				\dot{z} &= v_z\\
				\dot{v}_x &= \frac{T_x}{m}\\
				\dot{v}_y &= \frac{T_y}{m}\\
				\dot{v}_z &= \frac{T_z}{m} - g\\
				\dot{m} &= -\frac{||\bm{T}||}{I_{sp} \cdot g0}
			\end{aligned}
        \end{cases}~ BC: ~
        \begin{cases}
        	\begin{aligned}
	            x(0) &= x_0\\
	            y(0) &= y_0\\
	            z(0) &= z_0\\
	            v_x(0) &= {v_x}_0\\
	            v_y(0) &= {v_y}_0\\
	            v_z(0) &= {v_z}_0\\
	            m(0) &= m_0\\
        	\end{aligned}
        \end{cases} ~
        \begin{cases}
        	\begin{aligned}
	            x(t_f) &= 0\\
	            y(t_f) &= 0\\
	            z(t_f) &= 0\\
	            v_x(t_f) &= 0\\
	            v_y(t_f) &= 0\\
	            v_z(t_f) &= 0\\
	        \end{aligned}
        \end{cases} 
    \end{displaystyle}
    \label{eq:EoM3D_GPOPS}
\end{equation}

\noindent The goal is to train a network able to give for each state of the trajectory a control thrust $\bm{T}$, such that the lander reaches the final state minimizing the cost function:
\begin{equation} J_{FO} = \text{min} \int_0^{t_f} ||\bm{T}|| \ dt \end{equation}
where $\bm{T} = T_x \bm{\hat{x}} + T_y \bm{\hat{y}} + T_z \bm{\hat{z}}$. The module of the thrust vector $||\bm{T}||$ is constrained to be between 1000 N and 3400 N.

The input features are again position, velocity and mass. The target is a vector of 4 components: the first three $[t_x,t_y,t_z]$ are the unity components of the thrust and are numerical, while the last $t_c$ is categorical (0-1): 0 indicates minimum thrust and 1 maximum thrust.

\begin{itemize}
    \item Input features: $$X = [ x, y, z, v_x, v_y, v_z, m ] \hspace{1cm} X \in \mathbb{R}^{7 \times 1}$$
    \item Target: $$y = [t_x, t_y, t_z, t_c] \hspace{1cm} y \in \mathbb{R}^{4 \times 1}$$
\end{itemize}


\subsection{Train and test dataset generation}
For the FO 3D landing problem, 1000 optimal trajectory are generated with GPOPS for the train set, for a total of approximately 250.000 states (even in this case, it is not necessary to use all these states for the train set). The initial conditions for these trajectories are sampled uniformly randomly within the following ranges:

\begin{table}[H]
    \centering
    \begin{tabular}{ | p{3cm} | p{3cm} | p{3cm} | }
    \hline 
    \textbf{Variable} & \textbf{Min. value} & \textbf{Max. value}\\
    \hline
    $x_0$ & 1500 m & 2000 m\\
    $y_0$ & -100 m & 100 m\\
    $z_0$ & 1000 m & 1500 m\\
    ${v_x}_0$ & -60 m/s & -50 m/s\\
    ${v_y}_0$ & -10 m/s & 10 m/s\\
    ${v_z}_0$ & -30 m/s & -20 m/s\\
    $m$ & 1200 kg & 1400 kg\\
    \hline
    \end{tabular}
    \caption{EO 3D train set initial condition}
\end{table}

\noindent The train set is represented in Fig. \ref{fig:GPOPS_3D_train_set}, where the red volume indicates the zone where the initial conditions are sampled.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{img/GPOPS_3D_DNN_train_ds_0.png}
    \caption{FO 3D train set}
    \label{fig:GPOPS_3D_train_set}
\end{figure}


\subsection{Dynamics simulator}
% METTERE CONSTRAINT SU COMPONENTI THRUST
\noindent For each initial condition of the test trajectories:
\begin{enumerate}
    \item Initialize position $x = x_0$, $y = y_0$, $z = z_0$, velocity ${v_x} = {v_x}_0$, ${v_y} = {v_x}_0$, ${v_z} = {v_z}_0$ and mass $m = m_0$

    \item Apply MinMaxScaler to input features $X = [x_0, y_0, z_0, {v_x}_0, {v_y}_0, {v_z}_0, m_0]$, and predict $y_{pred} = [t_x, t_y, t_z, t_c]$

    \item Compute the thrust as $\bm{T} = [t_x \hat{\bm{x}}, \ t_y \hat{\bm{y}}, \ t_z \hat{\bm{z}}] \cdot T$, where $T = 1000 + 2400 \cdot t_c \ [N]$; integrate equation of motion \ref{eq:EoM3D_GPOPS} with a time step of 0.1 s

    \item If the altitude $z > 0 \ m$ and the vertical velocity $v_z < 0 \ m/s$ go back to point 2, otherwise interrupt integration
\end{enumerate}


\subsection{DAgger procedure}
\label{sec:GPOPS_3D_dagger_procedure}
\begin{enumerate}
    \item The train set is generated with GPOPS and the ML model is trained. 

    \item A Monte Carlo simulation is performed: the current policy is run with the test trajectories to obtain the predicted trajectories.

    \item The predicted trajectories are labeled with GPOPS. Since both regression and classification are implied, in order to collect the states for DAgger it is used the following criteria: the predicted states where the prediction error on the regression $y_{err} = ||[t_x, t_y, t_z]_{pred} - [t_x, t_y, t_z]_{opt}||$ is bigger than a threshold $\epsilon_{min}$ and below a threshold $\epsilon_{max}$ are collected; also, the predicted states where the classification is wrong ($[t_c]_{pred} \neq [t_c]_{opt}$) are collected. 

    \item The train set is augmented with the states collected in the previous point. The procedure is repeated going back to point 1 and training a new model on the augmented train set.
\end{enumerate}


\subsection{Deep Neural Network}
The original size of the 1D train set with 1000 trajectories is approximately 30.000
states. Like in the other cases, the network complexity increases over the DAgger iterations, as Fig. \ref{fig:FO_1D_DNN_architecture} shows.

\begin{figure}[H]
    \centering
    \subfigure[Iteration 0]
    {\includegraphics[width=0.49\linewidth]{../3D/GPOPS/DNN/dagger/model/DNN_0.png}}
    \subfigure[Iteration 4]
    {\includegraphics[width=0.49\linewidth]{../3D/GPOPS/DNN/dagger/model/DNN_3.png}}
    \caption{FO 3D DNN architecture}
    \label{fig:FO_3d_dnn_architecture}
\end{figure}


\noindent The following hyperparameters are common to all the architectures:
\begin{itemize}
    \item Learning rate: $10^{-3}$, decreases by a factor of 0.9 each time the validation loss does not decreases for 5 consecutive epochs.
    \item Mini-batch size: 8, same used by Izzo in \cite{izzo1}.
    \item Activation function: ReLU for all layers and outputs, except tanh for y output.
    \item Normalization: input features are rescaled in a range $[0, 100]$ using the MinMaxScaler of Keras.
\end{itemize}

\noindent The following hyperparameters, instead, are searched again for each iteration:
\begin{itemize}
    \item Number of hidden layers.
    \item Number of neurons per layer.
\end{itemize}


\noindent The performances of DNN over the DAgger iterations are reported in Table \ref{tab:FO_3d_dnn_results_dagger}, and compared to classical supervised learning in Fig. \ref{fig:FO_3d_dnn_results_dagger_vs_supervised}. The average final position and velocity are defined as following:\\

\noindent Pos: $\sqrt{x_f^2 + y_f^2 + z_f^2}$\\
Vel: $\sqrt{{v_x}_f^2 + {v_y}_f^2 + {v_z}_f^2}$

\begin{table}[H]
    \centering
    \begin{tabular}{ | p{1.5cm} | p{1.5cm} | p{1.5cm} | p{1.6cm} | p{1.5cm} | p{1.6cm} | p{1.5cm} | }
    \hline
    \rowcolor{DarkBlue}
    % \mc{1}{}  & \mc{1}{x} & \mc{1}{y} & \mc{1}{w} & \mc{1}{z} \\
    \hline
    \color{white}{Iter} & \color{white}{Val loss} & \color{white}{Pos $[m]$} & \color{white}{Vel $[m/s]$} & \color{white}{$D_{CM}$} & \color{white}{New data} & \color{white}{Data}\\
    
    0 & $0.08$ & $8.03$ & $3.91$ & $1.9 \cdot 10^{-3}$ & - & $9,117$ \\
    
    \rowcolor{Gray}
    1 & $0.05$ & $6.74$ & $1.94$ & $1.8 \cdot 10^{-3}$ & 397 & $9,514$ \\

    2 & $0.06$ & $6.54$ & $4.27$ & $1.7 \cdot 10^{-3}$ & 43 & $9,556$ \\
    
    \rowcolor{Gray}
    3 & $0.05$ & $1.54$ & $0.81$ & $1.3 \cdot 10^{-3}$ & 293 & $9,849$ \\

    4 & $0.09$ & $0.48$ & $1.02$ & $8.4 \cdot 10^{-4}$ & 905 & $10,754$ \\

    \hline
    \end{tabular}
    \caption{FO 3D DNN results DAgger}
    \label{tab:FO_3d_dnn_results_dagger}
\end{table}

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{../3D/GPOPS/DNN/results_dagger_vs_super.png}
    \caption{FO 3D DNN results DAgger vs Supervised. The subplots represent the validation loss, the average final position, the average final velocity and the Cramer-Von Mises distance.}
    \label{fig:FO_3d_dnn_results_dagger_vs_supervised}
\end{figure}
