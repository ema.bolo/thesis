\babel@toc {english}{}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {figure}{\numberline {2.1}{\ignorespaces The issue of sequential prediction problems in imitation learning. Image taken from \cite {levine_dagger}.\relax }}{9}{figure.caption.10}% 
\contentsline {figure}{\numberline {2.2}{\ignorespaces Scheme of recurrent and feedforward neural networks. Image taken from \cite {feedforward_recurrent}.\relax }}{11}{figure.caption.11}% 
\contentsline {figure}{\numberline {2.3}{\ignorespaces Scheme of a SLFN. Image taken from \cite {SLFN}.\relax }}{11}{figure.caption.12}% 
\contentsline {figure}{\numberline {2.4}{\ignorespaces Scheme of a SLFN with connections. Image taken from \cite {barocco}.\relax }}{15}{figure.caption.14}% 
\contentsline {figure}{\numberline {2.5}{\ignorespaces Example of regression curve\relax }}{17}{figure.caption.15}% 
\contentsline {figure}{\numberline {2.6}{\ignorespaces Confusion matrix. Image taken from \cite {confusion_matrix}.\relax }}{18}{figure.caption.16}% 
\addvspace {10\p@ }
\contentsline {figure}{\numberline {3.1}{\ignorespaces Example of ZEM/ZEV solution for 1D case\relax }}{23}{figure.caption.17}% 
\contentsline {figure}{\numberline {3.2}{\ignorespaces EO problem 1D train set\relax }}{26}{figure.caption.19}% 
\contentsline {figure}{\numberline {3.3}{\ignorespaces Collection criteria of the new states for DAgger\relax }}{28}{figure.caption.20}% 
\contentsline {figure}{\numberline {3.4}{\ignorespaces Predicted trajectories over train set, before and after DAgger\relax }}{30}{figure.caption.21}% 
\contentsline {figure}{\numberline {3.5}{\ignorespaces 1D State distribution, before and after DAgger\relax }}{31}{figure.caption.22}% 
\contentsline {figure}{\numberline {3.6}{\ignorespaces EO 1D DNN architecture\relax }}{32}{figure.caption.23}% 
\contentsline {figure}{\numberline {3.7}{\ignorespaces EO 1D DNN results DAgger vs Supervised. The subplots represent the validation loss, the average final position, the average final velocity and the Cramer-Von Mises distance.\relax }}{35}{figure.caption.25}% 
\contentsline {figure}{\numberline {3.8}{\ignorespaces EO 1D ELM architecture\relax }}{36}{figure.caption.26}% 
\contentsline {figure}{\numberline {3.9}{\ignorespaces EO 1D ELM results DAgger vs Supervised. The subplots represent the validation loss, the average final position, the average final velocity and the Cramer-Von Mises distance.\relax }}{37}{figure.caption.28}% 
\contentsline {figure}{\numberline {3.10}{\ignorespaces EO 3D train set\relax }}{40}{figure.caption.30}% 
\contentsline {figure}{\numberline {3.11}{\ignorespaces 3D State distribution, before and after DAgger\relax }}{42}{figure.caption.31}% 
\contentsline {figure}{\numberline {3.12}{\ignorespaces EO 3D DNN architecture\relax }}{43}{figure.caption.32}% 
\contentsline {figure}{\numberline {3.13}{\ignorespaces Choosing the best architecture with TensorBoard\relax }}{44}{figure.caption.33}% 
\contentsline {figure}{\numberline {3.14}{\ignorespaces EO 3D DNN example of regression curve\relax }}{45}{figure.caption.34}% 
\contentsline {figure}{\numberline {3.15}{\ignorespaces EO 3D DNN results DAgger vs Supervised. The subplots represent the validation loss, the average final position, the average final velocity and the Cramer-Von Mises distance.\relax }}{46}{figure.caption.36}% 
\contentsline {figure}{\numberline {3.16}{\ignorespaces EO 3D ELM architecture\relax }}{47}{figure.caption.37}% 
\contentsline {figure}{\numberline {3.17}{\ignorespaces EO 3D ELM example of regression curve\relax }}{47}{figure.caption.38}% 
\contentsline {figure}{\numberline {3.18}{\ignorespaces EO 3D ELM results DAgger vs Supervised. The subplots represent the validation loss, the average final position, the average final velocity and the Cramer-Von Mises distance.\relax }}{48}{figure.caption.40}% 
\addvspace {10\p@ }
\contentsline {figure}{\numberline {4.1}{\ignorespaces Examples of GPOPS solution for 1D case\relax }}{51}{figure.caption.41}% 
\contentsline {figure}{\numberline {4.2}{\ignorespaces FO 1D train set\relax }}{54}{figure.caption.43}% 
\contentsline {figure}{\numberline {4.3}{\ignorespaces Partial dependence plot 1\relax }}{55}{figure.caption.44}% 
\contentsline {figure}{\numberline {4.4}{\ignorespaces Partial dependence plot 2\relax }}{55}{figure.caption.45}% 
\contentsline {figure}{\numberline {4.5}{\ignorespaces Predicted trajectories over train set, before and after DAgger, 2D view\relax }}{58}{figure.caption.46}% 
\contentsline {figure}{\numberline {4.6}{\ignorespaces Predicted trajectories over train set, before and after DAgger, 3D view\relax }}{58}{figure.caption.47}% 
\contentsline {figure}{\numberline {4.7}{\ignorespaces FO 1D DNN architecture\relax }}{59}{figure.caption.48}% 
\contentsline {figure}{\numberline {4.8}{\ignorespaces FO 1D DNN Confusion matrix\relax }}{60}{figure.caption.49}% 
\contentsline {figure}{\numberline {4.9}{\ignorespaces FO 1D DNN results DAgger vs Supervised. The subplots represent the validation loss, the average final position, the average final velocity and the Cramer-Von Mises distance.\relax }}{61}{figure.caption.51}% 
\contentsline {figure}{\numberline {4.10}{\ignorespaces FO 1D ELM Confusion Matrix\relax }}{62}{figure.caption.52}% 
\contentsline {figure}{\numberline {4.11}{\ignorespaces FO 1D ELM State Distribution\relax }}{62}{figure.caption.53}% 
\contentsline {figure}{\numberline {4.12}{\ignorespaces FO 3D train set\relax }}{65}{figure.caption.55}% 
\contentsline {figure}{\numberline {4.13}{\ignorespaces FO 3D DNN architecture\relax }}{67}{figure.caption.56}% 
\contentsline {figure}{\numberline {4.14}{\ignorespaces FO 3D DNN results DAgger vs Supervised. The subplots represent the validation loss, the average final position, the average final velocity and the Cramer-Von Mises distance.\relax }}{69}{figure.caption.58}% 
\addvspace {10\p@ }
