Dagger iteration: 2

Train set points:	1.17E+04
Test RMSE:		3.32E-01

Monte Carlo simulation:
Success rate:		7.00 %
Average final position:	[28.71, 3.19, 0.52]
Average final velocity:	[6.04, 1.97, 8.60]

Distance CM:		4.42E-03