clear; close all; clc

ds_train_0 = csvread('ds_train_0.csv');
ds_train_3000 = csvread('ds_train_3000.csv');
new_data_vec = [604, 1107, 1942];


for i = 1:length(new_data_vec) 
    new_data = sum(new_data_vec(1:i));
    size_0 = size(ds_train_0,1);
    ds_train_i = ds_train_3000(1:size_0+new_data, :);
    
    size(ds_train_i)
    
    SAVE_PATH = strcat('ds_train_', num2str(i), '.csv');
    
    dlmwrite(SAVE_PATH, ds_train_i, 'delimiter', ',', 'precision', 8)
        
    logFileID = fopen(strcat('ds_train_', num2str(i), '.txt'), 'w');
    fprintf(logFileID, 'Total number of points: %d', size(ds_train_i,1));
    fclose(logFileID);
end