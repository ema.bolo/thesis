clear; close all; clc
rng('shuffle')
addpath('../../common3D/matlab')

% Number of trajectories
N = 100;

FILE_NAME = 'ds_test_3';
SAVE_PATH = strcat(FILE_NAME, '.csv');
n_steps = 100;

aux = [];
try
    parpool
catch
    %
end
str = strcat('Generating ', num2str(N), ' trajectories... ');
ppm = ParforProgMon(str, N, 1, 500, 70);

parfor i = 1:N
    % Random BC
    x0_rand = unifrnd(1550, 1950);
    y0_rand = unifrnd(-90, 90);
    z0_rand = unifrnd(1050, 1450);
    vx0_rand = unifrnd(-58, -52);
    if y0_rand > 0
        vy0_rand = unifrnd(-8, 0);
    else
        vy0_rand = unifrnd(0, 8);
    end
    vz0_rand = unifrnd(-28, -22);
    BC = [x0_rand, y0_rand, z0_rand, vx0_rand, vy0_rand, vz0_rand];

    % Compute trajectory
    tic
    [state, acc, time] = ZEM_ZEV_3D(BC);    
    toc
    
    state = state(1:end-2, :);
    acc = acc(1:end-2, :);
    time = time(1:end-2);
    Mat_ds = [state, acc, time];
    
    % downsample
    sample_vec = linspace(1, length(time), n_steps);
    Mat_ds = Mat_ds(round(sample_vec), :);
    
    aux = [aux; Mat_ds];

%     figure()
%     subplot(411)
%     plot(time, state(:,1:3), tfix, Xfix(:,1:3),'.')
%     ylabel('Altitude [m]')    
%     subplot(412)
%     plot(time, state(:,4:6), tfix, Xfix(:,4:6),'.')
%     ylabel('Velocity [m/s]')    
%     subplot(413)
%     plot(time, state(:,7), tfix, Xfix(:,7),'.')
%     ylabel('tgo [kg]')    
%     subplot(414)
%     plot(time, acc, tfix, yfix,'.')
%     ylabel('acc [m/s^2]')
%     
%     figure()
%     grid on
%     hold on
%     plot3(state(:,1), state(:,2), state(:,3))
%     plot3(0,0,0,'rx')
%     xlabel('x')
%     ylabel('y')
%     zlabel('z')
     
    ppm.increment()
end

n_points = size(aux,1);

dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)

logFileID = fopen(strcat(FILE_NAME,'.txt'), 'w');
fprintf(logFileID, 'N: %d', N);
fprintf(logFileID, '\nn_points: %d', n_points);
fclose(logFileID);

rmpath('../../common3D/matlab')


%% plot 3D
close all
x = aux(:,1);
y = aux(:,2);
z = aux(:,3);

figure()
plot3(x, y, z, '.')
grid on
xlabel('x')
ylabel('y')
zlabel('z')
axis equal

return
y_x = aux(:,9);
y_y = aux(:,10);
y_z = aux(:,11);

figure()
hold on
plot(y_x,'-o')
plot(y_y,'-o')
% plot(y_z,'-o')
