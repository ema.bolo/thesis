clear; close all; clc
addpath('../../common3D/matlab')

it = 0;

BC2CORRECT = strcat('dagger/predictions/bc2correct_', num2str(it), '.csv');
CORRECTIONS = strcat('dagger/predictions/corrections_', num2str(it), '.csv');
TRAIN_DS = strcat('dagger/train_ds/ds_train_', num2str(it), '.csv');
PRED_TRAJ = strcat('dagger/predictions/predictedTrajectories_', num2str(it), '.csv');

train_ds = csvread(TRAIN_DS);
% BC_mat = csvread(BC2CORRECT);
% predTraj = csvread(PRED_TRAJ);


%% plot states to correct
figure()
hold on
grid on
plot3(train_ds(:,1), train_ds(:,2), train_ds(:,3), 'b.')
% plot3(BC_mat(:,2), BC_mat(:,3), BC_mat(:,4), 'r.')
% plot3(predTraj(:,1), predTraj(:,2), predTraj(:,3), 'r.')
axis equal
xlabel('x [m]')
ylabel('y [m]')
zlabel('z [m]')
% title(strcat('Train dataset, iter ', num2str(dagger_it)))
% legend('Train set', 'Predicted trajectories','Location','NorthWest')
plot_cube([1500, -100, 1000], 500, 200, 500, 'r')
xlim([-10 2000])
ylim([-300 300])
zlim([-5 1500])
view(3)
ax = gca;               % get the current axis
ax.Clipping = 'off';    % turn clipping off
return

%% correct labels
N = size(BC_mat, 1);

aux = [];
aux2 = []; % to keep skipped trajectories
skipped_trajectories = 0;

try
    parpool
catch
    %
end
str = strcat('Correcting ', num2str(N), ' states... ');
ppm = ParforProgMon(str, N, 1, 500, 70);

parfor i = 1:N
    BC = BC_mat(i, :);
    
    % Compute trajectory
    try
        tic
        [state, acc, time] = ZEM_ZEV_3D(BC(2:end));    
        toc
    catch
        disp('Trajectory skipped')
        skipped_trajectories = skipped_trajectories + 1;
        aux2 = [aux2; BC(1)];
        continue
    end
    
    Mat_ds = [state, acc, time];
    Mat_ds = [BC(1), Mat_ds(1, :)];
    
    aux = [aux; Mat_ds];
    
    ppm.increment()
end

fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)


%%
if ~isempty(aux2) % if aux2 is non empty vector
    aux2 = [aux2, zeros(size(aux2,1), 5)];
    aux = [aux; aux2];
end

aux = sortrows(aux,1);

aux = aux(:, 2:end); % remove first column
dlmwrite(CORRECTIONS, aux, 'delimiter', ',', 'precision', 8)
rmpath('../../common3D/matlab')
