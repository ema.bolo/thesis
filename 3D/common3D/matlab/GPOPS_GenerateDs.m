function GPOPS_GenerateDs(N, n_points, SAVE_PATH, TEXT_PATH, tol, LSTM)

rng('shuffle')

aux = [];
skipped_trajectories = 0;

z0_vec = linspace(900, 2000, N);
vz0_vec = linspace(-40, -10, N);

str = ['Generating dataset ... 0 / ', num2str(N*N)];
h = waitbar(0, str);

for i = 1:N
    for j = 1:N
    
        BC = [z0_vec(i), vz0_vec(j), 1300];
        BC = [1000,-20,1300];

        % Compute trajectory
        tic
        output = GPOPS_ComputeTrajectory(BC, tol);
        toc
        
%         plot_gpops_solution(output)

        % Extract solution
        solution = output.result.solution;
        time = solution.phase(1).time;
        state = solution.phase(1).state;
        control = solution.phase(1).control;

        % Preprocess data
        state = [state(:,3), state(:,6), state(:,7)];
        y = control(:,4);
        
        time(end)
        return

        % Uniform sequence length and time step
        [Xfix, yfix, tfix] = GPOPS_SampleFix(state, y, time, n_points);    
        Mat_ds = [Xfix, yfix, tfix];
        
        % Shuffle if not LSTM
        if ~LSTM
            Mat_ds = randblock(Mat_ds, [1, size(Mat_ds,2)]);
            Mat_ds = Mat_ds(1:800,:);
        end
        

    %     figure(1)
    %     subplot(211)
    %     grid on
    %     plot(time, state(:,1), 'bo', tfix, Xfix(:,1), 'r.')    
    %     subplot(212)
    %     grid on
    %     plot(time, y, 'bo', tfix, 1000+2400*yfix, 'r.')    
    %     close(h)
    %     return

        aux = [aux; Mat_ds];    

        str = ['Generating dataset ... ', num2str(i),' / ', num2str(N)];
        waitbar(i/N, h, str)
    
    end

end

close(h)

fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)

% Shuffle if not LSTM
if ~LSTM
    aux = randblock(aux, [1, size(aux,2)]);
end


dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)

logFileID = fopen(TEXT_PATH, 'w');
fprintf(logFileID, 'Trajectories: %d', N*N);
fprintf(logFileID, '\nPoints per trajectory: %d', 800);
fprintf(logFileID, '\nTotal points: %d', N*N*800);
fclose(logFileID);
end