function val_loss = Evaluate_ELM(aux)

test_ds = load(aux.TEST_DS);

%% Test
X_test = test_ds(:, 1:7);

scaler = load(aux.SCALER);
scaler = scaler.scaler;
X_test_norm = X_test./scaler;

y_test = test_ds(:, 9:11);

% load model
model = load(aux.MODEL);
model = model.model;

% test model
[~, ~, val_loss] = elm_predict_and_test(model, [y_test, X_test_norm]);

end