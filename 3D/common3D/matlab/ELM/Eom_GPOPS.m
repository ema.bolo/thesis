function dxdt = Eom(t, X, y_pred)
h = X(1);
vh = X(2);
m = X(3);

g0 = 1.622;
alphaM = 1/200/9.81;
T = 1000 + y_pred*2400;

hdot = vh;
vhdot = -g0 + T/m;
mdot = -alphaM*T;

dxdt = [hdot; vhdot; mdot];
end