function GetResults(aux, val_acc)

[SR, finalState] = AnalyzePredictedTrajectories(aux);
[D_CM_pos, D_CM_vel] = PlotHistograms1d(aux);

logFileID = fopen(aux.TEXT_FILE, 'w');
fprintf(logFileID, 'Val accuracy: %f\n', val_acc);
fprintf(logFileID, 'SR: %d\n', SR);
fprintf(logFileID, 'Final state: [%f, %f]\n', finalState(1), finalState(2));
fprintf(logFileID, 'D_CM_pos: %f\n', D_CM_pos);
fprintf(logFileID, 'D_CM_vel: %f\n', D_CM_vel);
fclose(logFileID);

end