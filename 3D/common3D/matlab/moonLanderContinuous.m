function phaseout = moonLanderContinuous(input)

% input
% input.phase(phasenumber).state
% input.phase(phasenumber).control
% input.phase(phasenumber).time
% input.phase(phasenumber).parameter
%
% input.auxdata = auxiliary information
%
% output
% phaseout(phasenumber).dynamics
% phaseout(phasenumber).path
% phaseout(phasenumber).integrand

g = input.auxdata.g;
Isp = input.auxdata.Isp;
g0 = input.auxdata.g0;
phi = input.auxdata.phi;

t = input.phase.time;
x = input.phase.state(:,1);
y = input.phase.state(:,2);
z = input.phase.state(:,3);
xdot = input.phase.state(:,4);
ydot = input.phase.state(:,5);
zdot = input.phase.state(:,6);
m = input.phase.state(:,7);

ux = input.phase.control(:,1);
uy = input.phase.control(:,2);
uz = input.phase.control(:,3);
T = input.phase.control(:,4);

dx = xdot;
dy = ydot;
dz = zdot;
ddx = ux.*T./m;
ddy = uy.*T./m;
ddz = -g + uz.*T./m;
dm = -T./(Isp*g0*cos(phi));

path1 = sqrt(ux.^2+uy.^2+uz.^2);

phaseout.dynamics = [dx , dy , dz , ddx , ddy , ddz , dm];
phaseout.path = path1;
phaseout.integrand = T;
