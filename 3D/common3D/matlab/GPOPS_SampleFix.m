function [Xfix, y_tfix, tfix] = GPOPS_SampleFix(X, y_t, tvect, n_steps)

tfix = linspace(tvect(1), tvect(end), n_steps); 
Xfix = zeros(n_steps, 3);                

[tvect, index] = unique(tvect); % remove doubles

for i = 1:3 % for each column of X
    col = X(:, i);
    Xfix(:, i) = interp1(tvect, col(index), tfix); % linear interpolation
end

y_tfix = interp1(tvect, y_t(index), tfix);

% output column vectors
y_tfix = y_tfix';
tfix = tfix';
end