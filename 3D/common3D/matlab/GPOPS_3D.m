function [state, y, time] = GPOPS_3D(BC, tol)

%----------------------- Lunar-Lander Problem -----------------------------%
%-------------------------------------------------------------------------%
    
iterations = 1;

%-------------------------------------------------------------------------%
%----------Provide Mesh Refinement Method and Initial Mesh ---------------%
%-------------------------------------------------------------------------%
mesh.method          = 'hp-PattersonRao';
mesh.tolerance       = tol;
mesh.maxiterations   = 50;
mesh.colpointsmin    = 2;
mesh.colpointsmax    = 20;
mesh.phase.colpoints = 15; % initialize colpoints

% N = 10;
% mesh.phase.fraction   = ones(1,N)/N;
% mesh.phase.colpoints =  6*ones(1,N);
    
%-------------------------------------------------------------------------%
%------------------ Provide Auxiliary Data for Problem -------------------%
%-------------------------------------------------------------------------%
auxdata.g = 1.622;
auxdata.Isp = 200;
auxdata.g0 = 9.81;
mDry = 500;
auxdata.phi = 0;

%-------------------------------------------------------------------------%
%----------------- Provide All Bounds for Problem ------------------------%
%-------------------------------------------------------------------------%
t0min = 0;   t0max = 0;
tfmin = 1;   tfmax = 100; % 40, 100

x0 = BC(1);   xf = 0;
y0 = BC(2);   yf = 0;
z0 = BC(3);   zf = 0.00001;

xdot0 = BC(4);   xdotf = 0;
ydot0 = BC(5);   ydotf = 0;
zdot0 = BC(6);   zdotf = 0;
m0 = BC(7);      mf = mDry; 

xmin = 0;        xmax =  x0;
ymin = -300;     ymax =  300;
zmin = 0;        zmax =  z0;
xDotMin = -100;  xDotMax = 0;
yDotMin = -100;  yDotMax = 100;
zDotMin = -100;  zDotMax = 0;
mMin = mDry;     mMax = m0;

uxMin=-1;   uxMax=1;
uyMin=-1;   uyMax=1;
uzMin=0;    uzMax=1;
TFullThrottle = 4000;
Tmin = 0.25*TFullThrottle;   Tmax = 0.85*TFullThrottle;

%-------------------------------------------------------------------------%
%----------------------- Setup for Problem Bounds ------------------------%
%-------------------------------------------------------------------------%

% Time
bounds.phase.initialtime.lower = t0min;
bounds.phase.initialtime.upper = t0max;

bounds.phase.finaltime.lower = tfmin;
bounds.phase.finaltime.upper = tfmax;

% State
bounds.phase.initialstate.lower = [x0, y0, z0, xdot0, ydot0, zdot0, m0];
bounds.phase.initialstate.upper = [x0, y0, z0, xdot0, ydot0, zdot0, m0];

bounds.phase.state.lower = [xmin, ymin, zmin, xDotMin, yDotMin, zDotMin, mMin];
bounds.phase.state.upper = [xmax, ymax, zmax, xDotMax, yDotMax, zDotMax, mMax];

bounds.phase.finalstate.lower = [xf, yf, zf, xdotf, ydotf, zdotf, mDry];
bounds.phase.finalstate.upper = [xf, yf, zf, xdotf, ydotf, zdotf, m0];

% Control
bounds.phase.control.lower = [uxMin, uyMin, uzMin, Tmin];
bounds.phase.control.upper = [uxMax, uyMax, uzMax, Tmax];

% Path
bounds.phase.path.lower = 1;
bounds.phase.path.upper = 1;

% Integral
bounds.phase.integral.lower = 0;
bounds.phase.integral.upper = 1500000;

%%%%%%%%%%%%%%%%%%%%%%%%%%

it = 0;
for i = 1:iterations
    it = it+1;
    if i>1
        %-------------------------------------------------------------------------%
        %---------------------- Provide Guess of Solution ------------------------%
        %-------------------------------------------------------------------------%
        load('output.mat') 
        mesh.phase.colpoints = output.meshColPoints+10;     % add 10 coll points
        guess.phase.state    = output.result.solution.phase(1).state;
        guess.phase.control  = output.result.solution.phase(1).control;
        guess.phase.time     = output.result.solution.phase(1).time;
        guess.phase.integral = output.result.solution.phase(1).integral;
    else 
        %-------------------------------------------------------------------------%
        %---------------------- Provide Guess of Solution ------------------------%
        %-------------------------------------------------------------------------%
        tGuess               = [t0min; 70];
        xGuess               = [x0; xf];
        yGuess               = [y0; yf];
        zGuess               = [z0; zf];
        xDotGuess            = [xdot0; xdotf];
        yDotGuess            = [ydot0; ydotf];
        zDotGuess            = [zdot0; zdotf];
        mGuess               = [m0; mf];
        uxGuess              = [uxMin; uxMax];
        uyGuess              = [uyMin; uyMax];
        uzGuess              = [uzMin; uzMax];
        TGuess               = [Tmin; Tmax];
        guess.phase.state    = [xGuess , yGuess , zGuess, xDotGuess, yDotGuess, zDotGuess , mGuess];
        guess.phase.control  = [uxGuess , uyGuess , uzGuess , TGuess];
        guess.phase.time     = tGuess;
        guess.phase.integral = 700000;
    end
    
    %-------------------------------------------------------------------------%
    %------------- Assemble Information into Problem Structure ---------------%        
    %-------------------------------------------------------------------------%
    
    % Required fields    
    setup.name                        = 'Soft-Moon-Landing';
    setup.functions.endpoint          = @moonLanderEndpoint;
    setup.functions.continuous        = @moonLanderContinuous;
    setup.bounds                      = bounds;
    setup.guess                       = guess;
    
    % Optional fields
    setup.auxdata                     = auxdata;
    setup.derivatives.supplier        = 'sparseCD';
    setup.derivatives.derivativelevel = 'second';
    setup.method                      = 'RPM-Differentiation';
    setup.mesh                        = mesh;
    setup.nlp.solver                  = 'ipopt';
    setup.displaylevel                = 0; 
    
    %-------------------------------------------------------------------------%
    %----------------------- Solve Problem Using GPOPS2 ----------------------%
    %-------------------------------------------------------------------------%
    output = gpops2(setup);
    
    output.meshColPoints=mesh.phase.colpoints; % keep track of col points
    output.iterations=it;
%     save('output.mat')
end

    % Extract solution
    solution = output.result.solution;
    time = solution.phase(1).time;
    state = solution.phase(1).state;
    y = solution.phase(1).control;    
   
    % remove middle point
    Tmin = 1000; Tmax = 3400;
    aux = y(:,4);
    state(aux>Tmin & aux<Tmax, :) = [];
    time(aux>Tmin & aux<Tmax) = [];
    y(aux>Tmin & aux<Tmax, :) = [];
    
    % Thrust is either 0 or 1
    lim = (Tmin+Tmax)/2;
    y(:,4) = (y(:,4)>lim)*1;
end