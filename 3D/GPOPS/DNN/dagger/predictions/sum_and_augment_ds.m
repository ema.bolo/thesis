clear; close all; clc
rng('shuffle')
addpath('../../../../common3D/matlab')

dagger_it = 3;

CORRECTIONS = strcat('bc4dagger_', num2str(dagger_it), '.csv');
OLD_TRAIN_DS = strcat('../train_ds/ds_train_', num2str(dagger_it), '.csv'); 
NEW_TRAIN_DS = strcat('../train_ds/ds_train_', num2str(dagger_it+1), '.csv');
TEXT = strcat('../train_ds/ds_train_', num2str(dagger_it+1), '.txt');

BC_mat_1 = csvread('aux_temp_1.csv');
BC_mat_2 = csvread('aux_temp_2.csv');
BC_mat_3 = csvread('aux_temp_3.csv');
BC_mat_4 = csvread('aux_temp_4.csv');
% BC_mat_5 = csvread('aux_temp_5.csv');

size(BC_mat_1)
size(BC_mat_2)
size(BC_mat_3)
size(BC_mat_4)
% size(BC_mat_5)

old_train_ds = csvread(OLD_TRAIN_DS);
aux = [BC_mat_1; BC_mat_2; BC_mat_3; BC_mat_4];

size(aux)
aux(aux(:,end)>0, :) = [];
size(aux)

aux(:,11) = 1; % set maximum thrust

%% plot new states
figure()
hold on
plot3(old_train_ds(1:10:end,1), old_train_ds(1:10:end,2), old_train_ds(1:10:end,3),'b.')
plot3(aux(:,1), aux(:,2), aux(:,3),'r.')
xlabel('Position [m]')
ylabel('Velocity [m/s]')
xlim([0, 2000])
ylim([-200, 200])
zlim([0, 1500])
grid on
axis equal


%% augment old train dataset
ds_augmented = [old_train_ds; aux];
ds_augmented_shuffled = randblock(ds_augmented, [1, size(aux,2)]); % shuffle

fprintf('\nOld training set: %d states\n', size(old_train_ds,1))
fprintf('New states: %d states\n', size(aux,1));
fprintf('Augmented training set: %d states\n', size(ds_augmented,1))

logFileID = fopen(TEXT, 'w');
fprintf(logFileID, 'Old training set: %d states\n', size(old_train_ds,1));
fprintf(logFileID, 'New states: %d states\n', size(aux,1));
fprintf(logFileID, 'Augmented training set: %d states\n', size(ds_augmented,1));
fclose(logFileID);

dlmwrite(NEW_TRAIN_DS, ds_augmented_shuffled, 'delimiter', ',', 'precision', 8)
dlmwrite(CORRECTIONS, aux, 'delimiter', ',', 'precision', 8)
rmpath('../../../../common3D/matlab')