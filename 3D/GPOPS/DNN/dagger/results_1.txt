Dagger iteration: 1

Train set points:	3.21E+04
Test RMSE:		5.19E-02
Test accuracy:		1.00

Monte Carlo simulation:
Success rate:		34.00 %
Average final position:	[3.72, 1.47, 5.43]
Average final velocity:	[0.44, 0.25, 0.14]

Distance CM:		9.44E-04