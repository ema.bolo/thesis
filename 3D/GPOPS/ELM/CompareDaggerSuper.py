import numpy as np
import pandas as pd
import keras
import matplotlib.pyplot as plt

import sys
sys.path.insert(0, '../../common3D')
import Eom, util, util_plot

def Compare(n_iter):
    dagger_results = np.zeros((n_iter+1, 9))
    super_results = np.zeros((n_iter+1, 9))

    for i in range(n_iter+1):
        dagger_results[i, :] = np.load('dagger/results_' + str(i) + '.npy')
        super_results[i, :] = np.load('super/results_' + str(i) + '.npy')

    x = np.linspace(0, n_iter, n_iter+1)

    fig = plt.figure(num=None, figsize=(10, 7))
    plt.subplot(411)
    plt.grid()
    plt.plot(x, dagger_results[:,0], '-o')
    plt.plot(x, super_results[:,0], '-o')
    plt.ylabel('Test loss')

    plt.subplot(412)
    plt.grid()
    finalPos_dagger = np.sqrt(dagger_results[:,3]**2+dagger_results[:,4]**2+dagger_results[:,5]**2)
    finalPos_super = np.sqrt(super_results[:,3]**2+super_results[:,4]**2+super_results[:,5]**2)
    plt.plot(x, finalPos_dagger, '-o')
    plt.plot(x, finalPos_super, '-o')
    plt.ylabel('Final position [m]')

    plt.subplot(413)
    plt.grid()
    finalVel_dagger = np.sqrt(dagger_results[:,6]**2+dagger_results[:,7]**2+dagger_results[:,8]**2)
    finalVel_super = np.sqrt(super_results[:,6]**2+super_results[:,7]**2+super_results[:,8]**2)
    plt.plot(x, finalPos_dagger, '-o')
    plt.plot(x, finalPos_super, '-o')
    plt.ylabel('Final velocity [m/s]')

    plt.subplot(414)
    plt.grid()
    plt.plot(x, dagger_results[:,8], '-o', label='Dagger')
    plt.plot(x, super_results[:,8], '-o', label='Supervised')
    plt.ylabel('Von Mises Distance')
    plt.xlabel('Dagger iteration')

    fig.legend(bbox_to_anchor=(0, 1), loc='upper left', ncol=1)

    fig.savefig('results_dagger_vs_super.png')
    plt.show()


def OnlyDagger(n_iter):
    dagger_results = np.zeros((n_iter+1, 10))

    for i in range(n_iter+1):
        dagger_results[i, :] = np.load('dagger/results_' + str(i) + '.npy')

    x = np.linspace(0, n_iter, n_iter+1)

    fig = plt.figure(num=None, figsize=(10, 7))
    plt.subplot(411)
    plt.grid()
    plt.plot(x, dagger_results[:,0], '-o')
    plt.ylabel('Test loss')

    plt.subplot(412)
    plt.grid()
    finalPos_dagger = np.sqrt(dagger_results[:,3]**2+dagger_results[:,4]**2+dagger_results[:,5]**2)
    plt.plot(x, finalPos_dagger, '-o')
    plt.ylabel('Final position [m]')

    plt.subplot(413)
    plt.grid()
    finalVel_dagger = np.sqrt(dagger_results[:,6]**2+dagger_results[:,7]**2+dagger_results[:,8]**2)
    plt.plot(x, finalPos_dagger, '-o')
    plt.ylabel('Final velocity [m/s]')

    plt.subplot(414)
    plt.grid()
    plt.plot(x, dagger_results[:,9], '-o', label='Dagger')
    plt.ylabel('Von Mises Distance')
    plt.xlabel('Dagger iteration')

    fig.legend(bbox_to_anchor=(0, 1), loc='upper left', ncol=1)

    fig.savefig('dagger/img/dagger_results.png')
    plt.show()


def CreateTable(n_iter, train_data_vec):
    dagger_results = np.zeros((n_iter+1, 10))

    for i in range(n_iter+1):
        dagger_results[i, :] = np.load('dagger/results_' + str(i) + '.npy')


    dagger_results = np.c_[np.arange(0,n_iter+1,1), dagger_results, train_data_vec]

    # final position
    xf = dagger_results[:,4]
    yf = dagger_results[:,5]
    zf = dagger_results[:,6]
    finalPos = np.sqrt(xf**2 + yf**2 + zf**2)

    # final velocity
    vxf = dagger_results[:,7]
    vyf = dagger_results[:,8]
    vzf = dagger_results[:,9]
    finalVel = np.sqrt(vxf**2 + vyf**2 + vzf**2)

    df = pd.DataFrame(dagger_results)
    df = df.drop(df.columns[[4,5,6,7,8,9]], axis=1)
    df.insert(4, "Final Pos [m]", finalPos)
    df.insert(5, "Final Vel [m/s]", finalVel)
    df.columns = ['Dagger iter','Val loss','Acc','SR','Final Pos [m]','Final Vel [m/s]','D_CM','N train data']
    df = df.drop(['SR'], axis=1)
    # pd.set_option('display.float_format', lambda x: '%.1f' % x)
    # pd.options.display.float_format = '{:.2E}'.format
    df['Dagger iter'] = df['Dagger iter'].round(0)
    df['Final Pos [m]'] = df['Final Pos [m]'].round(2)
    df['Final Vel [m/s]'] = df['Final Vel [m/s]'].round(2)

    fig, ax = util_plot.render_mpl_table(df, col_width=4.0)
    fig.savefig('dagger/img/dagger_results_tab.png')
    print(df)


if __name__ == "__main__":
    it = 3

    train_data_vec = []
    for i in range(it+1):
        TRAIN_PATH = 'dagger/train_ds/ds_train_' + str(i) + '.csv'
        temp = pd.read_csv(TRAIN_PATH, header=None)
        train_data_vec.append(temp.shape[0])

    # Compare(it)
    # OnlyDagger(it)
    CreateTable(it, train_data_vec)
