Dagger iteration: 4

Train set points:	3.53E+04
Test RMSE:		5.36E-02
Test accuracy:		1.00

Monte Carlo simulation:
Success rate:		2.00 %
Average final position:	[17.73, 5.10, 13.68]
Average final velocity:	[0.88, 0.45, 0.00]

Distance CM:		5.53E-03