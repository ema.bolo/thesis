clear; close all; clc
addpath('../../../../common3D/matlab')

dagger_it = 1;

CORRECTIONS = strcat('bc4dagger_', num2str(dagger_it), '.csv');
TRAIN_DS = strcat('../train_ds/ds_train_', num2str(dagger_it), '.csv'); 

train_ds = csvread(TRAIN_DS);


%% plot new states
figure()
hold on
for i = 1:5:size(train_ds, 1)
    if train_ds(i, 11)
        plot3(train_ds(i,1), train_ds(i,2), train_ds(i,3), 'r.')
    else
        plot3(train_ds(i,1), train_ds(i,2), train_ds(i,3), 'b.')
    end
end


xlabel('x [m]')
ylabel('y [m]')
zlabel('z [m]')
xlim([0, 2000])
ylim([-200, 200])
zlim([0, 1500])
grid on
axis equal

