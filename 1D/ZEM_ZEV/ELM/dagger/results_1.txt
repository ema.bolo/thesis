Dagger iteration: 1

Train set size: 10120 points
Test loss:		2.88E-04

Monte Carlo simulation:
Success rate:			98.00 %
Average final state:		[0.01, 0.09]
Distance CM:			3.87E-04