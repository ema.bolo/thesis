clear
clc
close all


ds_500 = csvread('ds_train_500.csv');
ds_reduced = ds_500(1:5000, :);

csvwrite('ds_train_0.csv', ds_reduced)


figure()
hold on
grid on
plot(ds_reduced(:,1),ds_reduced(:,2),'b.')
