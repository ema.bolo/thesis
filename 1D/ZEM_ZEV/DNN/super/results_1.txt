Dagger iteration: 1

Train set size: 20724 points
Test loss:		9.89E-03

Monte Carlo simulation:
Success rate:			42.00 %
Average final state:		[0.34, 1.49]
Distance CM:			2.01E-04