clc
clear
close all

iter = 5;
old_train_ds = csvread(strcat('dagger/train_ds/ds_train_', num2str(iter-1), '.csv'));
new_traj = csvread(strcat('dagger/train_ds/new_trajectories_', num2str(iter), '.csv'));


%% analyze new trajectories
PlotDataset(old_train_ds, new_traj)


figure()
subplot(211)
hold on
edges = linspace(round(min(old_train_ds(:,1))),... 
    round(max(old_train_ds(:,1))), 200);
histogram(old_train_ds(:,1), edges)
histogram(new_traj(:,1), edges)

subplot(212)
hold on
edges = linspace(round(min(old_train_ds(:,2))),... 
    round(max(old_train_ds(:,2))), 200);
histogram(old_train_ds(:,2), edges)
histogram(new_traj(:,2), edges)

%%

fprintf('Max altitude:\n')
fprintf('%.4f\n', max(old_train_ds(:,1)))
fprintf('%.4f\n\n', max(new_traj(:,1)))

fprintf('Min altitude:\n')
fprintf('%.4f\n', min(old_train_ds(:,1)))
fprintf('%.4f\n\n', min(new_traj(:,1)))

fprintf('Max velocity:\n')
fprintf('%.4f\n', max(old_train_ds(:,2)))
fprintf('%.4f\n\n', max(new_traj(:,2)))

fprintf('Min velocity:\n')
fprintf('%.4f\n', min(old_train_ds(:,2)))
fprintf('%.4f\n\n', min(new_traj(:,2)))

fprintf('Max acc:\n')
fprintf('%.4f\n', max(old_train_ds(:,4)))
fprintf('%.4f\n\n', max(new_traj(:,4)))

fprintf('Min acc:\n')
fprintf('%.4f\n', min(old_train_ds(:,4)))
fprintf('%.4f\n\n', min(new_traj(:,4)))