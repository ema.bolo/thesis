clear; close all; clc
rng('shuffle')

% Number of trajectories
N = 50;
x_vec = linspace(0.5, 1500, N);
y_vec = linspace(-50, -0.5, N);

y_true = zeros(N);

try
    parpool
catch
    %
end
str = strcat('Generating ', num2str(N), ' trajectories... ');
ppm = ParforProgMon(str, N, 1, 500, 70);

parfor i = 1:N
    for j = 1:N
        % Compute trajectory
        tic
        [state, acc, time] = ZEM_ZEV_1D([x_vec(i), y_vec(j), 1]);    
        toc

        y_true(i, j) = acc(1);
    end
    ppm.increment()
end

dlmwrite('y_true.csv', y_true, 'delimiter', ',', 'precision', 8)



%% plot 3d
figure
hold on
for i = 1:N
    for j = 1:N
        plot3(x_vec(i), y_vec(j), y_true(i,j), '.')
    end
end
grid on
xlabel('x')
ylabel('y')
zlabel('z')