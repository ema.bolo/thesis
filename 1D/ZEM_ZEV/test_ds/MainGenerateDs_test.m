clear; close all; clc
rng('shuffle')
addpath('../../common1D/matlab')

iter = 4;

ROOT = '../../common1D/common_bc/bc_test_';

bc_test = csvread(strcat(ROOT, num2str(iter), '.csv'));
N = size(bc_test, 1);

SAVE_PATH = strcat('ds_test_', num2str(iter), '.csv');
n_points = 100;

aux = [];

% try
%     parpool
% catch
%     %
% end
% str = strcat('Generating ', num2str(N), ' trajectories... ');
% ppm = ParforProgMon(str, N, 1, 500, 70);
for i = 1:N
    BC = bc_test(i, :);

    % Compute trajectory
    tic
    [state, acc, time] = ZEM_ZEV_1D(BC);    
    toc
    
    state = state(1:end-2,:);
    acc = acc(1:end-2);
    time = time(1:end-2);
      
    % Uniform sequence length and time step
    [Xfix, yfix, tfix] = ZEM_ZEV_SampleFix(state, acc, time, n_points);    
    Mat_ds = [Xfix, yfix, tfix];
    
    aux = [aux; Mat_ds];  

    figure()
    subplot(411)
    plot(time, state(:,1), tfix, Xfix(:,1),'b.')
    ylabel('Position [m]')    
    subplot(412)
    plot(time, state(:,2), tfix, Xfix(:,2),'b.')
    ylabel('Velocity [m/s]')    
    subplot(413)
    plot(time, state(:,3), tfix, Xfix(:,3),'b.')
    ylabel('t_{go} [kg]')    
    subplot(414)
    plot(time, acc, tfix, yfix(:,1),'b.')
    ylabel('Output [m/s^2]')
    return
    ppm.increment()
end

dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)
rmpath('../../common1D/matlab')

%% plot 3d
close all
x = aux(:,1);
y = aux(:,2);
z = aux(:,5);
figure()
plot3(x, y, z, '.')
grid on
xlabel('x')
ylabel('y')
zlabel('z')