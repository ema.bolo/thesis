clear; close all; clc
rng('shuffle')

iter = 1;

predictedTrajectories = csvread(strcat(...
    'dagger/predictions/predictedTrajectories_', num2str(iter), '.csv'));


pos_pred = predictedTrajectories(:,1);
vel_pred = predictedTrajectories(:,2);
mass_pred = predictedTrajectories(:,3);
y_pred = predictedTrajectories(:,4);


figure()
hold on
grid on
for i = 1:5:size(predictedTrajectories,1)
    if y_pred(i)
        plot3(pos_pred(i), vel_pred(i), mass_pred(i), 'r.')
    else
        plot3(pos_pred(i), vel_pred(i), mass_pred(i), 'b.')
    end
end
xlabel('pos')
ylabel('vel')
zlabel('mass')