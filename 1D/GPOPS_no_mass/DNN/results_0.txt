Dagger iteration: 0

Train set size: 9170 points
Validation loss:		1.66E-02

Monte Carlo simulation:
Accuracy:		0.97
Success rate:			76.00 %
Average final state:		[6.37, 0.12]
Distance CM position:		4.97E-04
Distance CM velocity:		3.62E-05