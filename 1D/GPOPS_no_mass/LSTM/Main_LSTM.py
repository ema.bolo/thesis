import numpy as np
import pandas as pd
import keras
from keras.models import Model, load_model
from keras.layers import Input
from keras.layers.core import Dense
from keras.layers.recurrent import LSTM
from sklearn.preprocessing import MinMaxScaler
import pickle
import datetime

import sys
sys.path.insert(0, '../../common1D')
import Eom, util, util_plot

iter = '0'

TRAIN_DS = 'dagger/train_ds/ds_train_' + iter + '.csv'

MODEL =     'dagger/model/LSTM_' + iter + '.h5'
SCALER =   'dagger/model/scaler_LSTM_' + iter + '.p'
TRAIN_LOG = 'dagger/model/training_LSTM_' + iter + '.csv'

# =============================================================================
#  Data
# =============================================================================
train_ds = pd.read_csv(TRAIN_DS, header=None)
x_train = train_ds.iloc[:, :2].values      # state
y_t_train = train_ds.iloc[:, 3].values     # thrust on/off

# Standardization/normalization
scaler = MinMaxScaler(feature_range=(0,100))
x_train = scaler.fit_transform(x_train)

pickle.dump(scaler, open(SCALER, "wb"))

# =============================================================================
# Parameters
# =============================================================================
mini_batch_size = 7
hidden_neurons = 32
n_features = 2

num_classes = 2

y_c_train = keras.utils.to_categorical(y_t_train, num_classes) # label for classification

n_point = 100
n_point_test = n_point - mini_batch_size + 1

n_traj_train = int(x_train[:, 0].size/n_point)
x_train = np.reshape(x_train, [n_traj_train, n_point, n_features])

# subdivision of the input data in mini batches with dimension 3
i = 0
dim_train = (n_traj_train*(n_point)) - ((mini_batch_size-1)*n_traj_train) # avoid to include first 2 points of each trajectory
X_train = np.zeros([dim_train, mini_batch_size, n_features])
for k in range(n_traj_train):
    for j in range(n_point_test):
        X_train[i, :, :]= x_train[k, j:j+mini_batch_size, :]
        i = i+1
    i = i

# Classification
y_class_train = np.reshape(y_c_train, [n_traj_train, n_point, num_classes])
y_cla_train = y_class_train[:, (mini_batch_size-1):, :]
Y_c_train = np.reshape(y_cla_train, [-1, num_classes])

# =============================================================================
# LSTM for classification and regression
# =============================================================================
initializer = keras.initializers.uniform()
regularizer = keras.regularizers.l2(0.0001)

visible = Input(shape=(mini_batch_size, n_features), name='main_input')

LSTM = LSTM(hidden_neurons, activation='relu',
    kernel_initializer=initializer, bias_initializer='zeros',
    kernel_regularizer=regularizer, name='LSTM')(visible)

clas_output = Dense(num_classes, activation='sigmoid', name='clas_output')(LSTM)

model = Model(inputs=[visible], outputs=[clas_output])
# model.summary()

# =============================================================================
# Train model
# =============================================================================
opt = keras.optimizers.Adam(lr=0.0001)
# opt = keras.optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0001)

model.compile(loss={'clas_output': "binary_crossentropy"},
              metrics={'clas_output':'accuracy'},
              optimizer=opt)

call_backs_list = [
        keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=1e-6, patience=10),
        keras.callbacks.ModelCheckpoint(monitor='val_loss', filepath=MODEL, save_best_only=True),
        keras.callbacks.CSVLogger(TRAIN_LOG, separator=',', append=False),
        keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=3)
        # keras.callbacks.TensorBoard(log_dir=TSB_LOG_DIR_PATH, histogram_freq=5),
        ]

history = model.fit({'main_input': X_train},
                    {'clas_output': Y_c_train},
                    epochs=500, batch_size=8, validation_split=0.15,
                    callbacks=call_backs_list)
