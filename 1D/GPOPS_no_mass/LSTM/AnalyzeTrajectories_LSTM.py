import sys
import pandas as pd
sys.path.insert(0, '../common')
import util, util_plot, AnalyzeTrajectories


if __name__ == "__main__":
    ROOT = '../dataset/test_ds/'
    TEST_DS_PATH = ROOT+'ds_test_10.csv'
    TEST_DS_DAGGER_PATH = ROOT+'test_ds_500_dagger_1.csv'
    PRED_TRAJ_PATH = ROOT+'predictedTrajectories_LSTM.npy'
    PRED_TRAJ_DAGGER_PATH = ROOT+'predictedTrajectories_LSTM_dagger_1.npy'
    SAVE_BC_PATH = '../dataset/train_ds/BC_for_dagger_LSTM.csv'


    val_ds = pd.read_csv(TEST_DS_PATH, header=None)
    x_val = val_ds.iloc[:, :2].values      # state
    y_t_val = val_ds.iloc[:, 3].values     # thrust on/off


    AnalyzeTrajectories.AnalyzePredictedTrajectories(PRED_TRAJ_PATH, TEST_DS_PATH, 100, LSTM=True)
    # AnalyzeTrajectories.AnalyzePredictedTrajectories(PRED_TRAJ_DAGGER_PATH, TEST_DS_DAGGER_PATH, LSTM=True)

    # AnalyzeTrajectories.AnalyzeBadTrajectoriesAccuracy(PRED_TRAJ_PATH, TEST_DS_PATH, [2, 0, 0], 1, 0, SAVE_BC_PATH, LSTM=True)
    # AnalyzeTrajectories.AnalyzeBadTrajectoriesAccuracy(PRED_TRAJ_DAGGER_PATH, TEST_DS_PATH, 0.967, 10.9, 10, 1, 0, 0, SAVE_BC_PATH, LSTM=True)

    # AnalyzeTrajectories.AnalyzeBadTrajectoriesFinalState(PRED_TRAJ_PATH, TEST_DS_PATH, [20, 1], 0, 0, SAVE_BC_PATH, LSTM=True)
    # AnalyzeTrajectories.AnalyzeBadTrajectoriesFinalState(PRED_TRAJ_DAGGER_PATH, TEST_DS_PATH, [20, 1], 0, 0, SAVE_BC_PATH, LSTM=True)

    # util_plot.PlotTrajectoryDistribution(TEST_DS_PATH, PRED_TRAJ_PATH, TEST_DS_DAGGER_PATH, PRED_TRAJ_DAGGER_PATH)
    # util_plot.PlotStateDistribution(TEST_DS_PATH, PRED_TRAJ_PATH)
    # util_plot.PlotStateDistribution(TEST_DS_DAGGER_PATH, PRED_TRAJ_DAGGER_PATH)
    # util_plot.PlotErrorDistribution(TEST_DS_PATH, PRED_TRAJ_PATH, TEST_DS_DAGGER_PATH, PRED_TRAJ_DAGGER_PATH)
