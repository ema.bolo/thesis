clear; close all; clc
rng('shuffle')

% Number of trajectories
N = 50;
tol = 1e-7;
n_points = 100;

FILE_NAME = 'ds_test_6';
SAVE_PATH = strcat(FILE_NAME, '.csv');

aux = [];

try
    parpool
catch
    %
end
str = strcat('Generating ', num2str(N), ' trajectories... ');
ppm = ParforProgMon(str, N, 1, 500, 60);

parfor i = 1:N
    % Random BC
    z0_rand = unifrnd(1000, 1500);
    vz0_rand = unifrnd(-30, -20);
    m0_vec = unifrnd(1000, 1500);
    BC = [z0_rand, vz0_rand, m0_vec];

    % Compute trajectory
    tic
    [X, y, time] = GPOPS_1D(BC, tol);
    toc

    % Uniform sequence length and time step
    % NB THE THRUST IS WRONG BECOUSE OF INTERPOLATION
    [Xfix, yfix, tfix] = GPOPS_SampleFix(X, y, time, n_points);    
    Mat_ds = [Xfix, yfix, tfix];

    aux = [aux; Mat_ds];
    
    figure()
    subplot(411)
    plot(time, X(:,1),'.')
    ylabel('Altitude [m]')    
    subplot(412)
    plot(time, X(:,2),'.')
    ylabel('Velocity [m/s]')    
    subplot(413)
    plot(time, X(:,3),'.')
    ylabel('Mass [kg]')    
    subplot(414)
    plot(time, y,'.')
    ylabel('Acceleration [m/s^2]')
    ppm.increment()
end

n_points = size(aux,1);


%%
dlmwrite(SAVE_PATH, aux, 'delimiter', ',', 'precision', 8)

logFileID = fopen(strcat(FILE_NAME,'.txt'), 'w');
fprintf(logFileID, 'Number of trajectories: %d', N);
fprintf(logFileID, '\nTotal number of points: %d', n_points);
fclose(logFileID);

