import subprocess

ROOT = 'python Main_DNN_dagger.py '
cases = []
cases.append('--layers 2 --n_hidden 32 --hyper_mode True')
cases.append('--layers 3 --n_hidden 32 --hyper_mode True')
cases.append('--layers 4 --n_hidden 32 --hyper_mode True')

cases.append('--layers 2 --n_hidden 64 --hyper_mode True')
cases.append('--layers 3 --n_hidden 64 --hyper_mode True')
cases.append('--layers 4 --n_hidden 64 --hyper_mode True')

cases.append('--layers 2 --n_hidden 128 --hyper_mode True')
cases.append('--layers 3 --n_hidden 128 --hyper_mode True')
cases.append('--layers 4 --n_hidden 128 --hyper_mode True')

for i in cases:
    subprocess.call(ROOT + i)

# tensorboard --logdir=path/to/log-directory
