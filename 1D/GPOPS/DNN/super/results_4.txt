Dagger iteration: 6

Train set size: 10717 points
Test loss:		9.97E-01

Monte Carlo simulation:
Success rate:			16.00 %
Average final state:		[10.57, 3.91]
Distance CM:			1.68E-03