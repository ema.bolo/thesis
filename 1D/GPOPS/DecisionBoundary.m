clear; close all; clc

% xy = -2.5 + 5*gallery('uniformdata',[200 2],0);
% x = xy(:,1);
% y = xy(:,2);
% v = x.*exp(-x.^2-y.^2);
% 
% [xq,yq] = meshgrid(-2:.2:2, -2:.2:2);
% vq = griddata(x,y,v,xq,yq);
% 
% mesh(xq,yq,vq)
% hold on
% plot3(x,y,v,'o')
% xlim([-2.7 2.7])
% ylim([-2.7 2.7])
% return

% [x,y,z,v] = flow;
% p = patch(isosurface(x,y,z,v,-3));
% isonormals(x,y,z,v,p)
% p.FaceColor = 'red';
% p.EdgeColor = 'none';
% daspect([1 1 1])
% view(3); 
% axis tight
% camlight 
% lighting gouraud


train_ds = csvread('DNN/dagger/train_ds/ds_train_0.csv');
k = 100;
x = train_ds(1:k,1);
y = train_ds(1:k,2);
z = train_ds(1:k,3);
y_train = train_ds(1:k,4);

[X,Y,Z] = meshgrid(...
    linspace(0,1500,100),...
    linspace(-42,-0.2,100),...
    linspace(1130,1400,100));

vq = griddata(x,y,z,y_train,X,Y,Z);

p = patch(isosurface(X,Y,Z,vq,1));

isonormals(X,Y,Z,vq,p)
p.FaceColor = 'red';
p.EdgeColor = 'blue';
view(3); 
axis tight
camlight 
lighting gouraud


hold on
grid on
for i = 1:2:size(train_ds,1)
    if train_ds(i,4)
        plot3(train_ds(i,1), train_ds(i,2), train_ds(i,3), 'r.')
    else
        plot3(train_ds(i,1), train_ds(i,2), train_ds(i,3), 'b.')
    end
end
xlabel('pos')
ylabel('vel')
zlabel('mass')



%% Predicted trajectories

% iter = 1;
% 
% predictedTrajectories = csvread(strcat(...
%     'DNN/dagger/predictions/predictedTrajectories_', num2str(iter), '.csv'));
% 
% pos_pred = predictedTrajectories(:,1);
% vel_pred = predictedTrajectories(:,2);
% mass_pred = predictedTrajectories(:,3);
% y_pred = predictedTrajectories(:,4);
% 
% figure()
% hold on
% grid on
% for i = 1:5:size(predictedTrajectories,1)
%     if y_pred(i)
%         plot3(pos_pred(i), vel_pred(i), mass_pred(i), 'r.')
%     else
%         plot3(pos_pred(i), vel_pred(i), mass_pred(i), 'b.')
%     end
% end
% xlabel('pos')
% ylabel('vel')
% zlabel('mass')