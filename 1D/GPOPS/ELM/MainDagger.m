clear
close all
clc

dagger_it = 1;
tol = 1e-2;

ROOT = 'dagger/train_ds/';
TRAIN_PATH = strcat(ROOT, 'ds_train_', num2str(dagger_it), '.csv'); 
SAVE_PATH = strcat(ROOT, 'ds_train_', num2str(dagger_it+1), '.csv');
TXT_PATH = strcat(ROOT, 'ds_train_', num2str(dagger_it+1), '.txt');
BC_PATH = strcat(ROOT, 'bc4dagger_', num2str(dagger_it), '.csv');

BC_mat = csvread(BC_PATH);
old_train_ds = csvread(TRAIN_PATH);

skipped_trajectories = 0;

%% plot old train set and new states
figure()
hold on
plot3(old_train_ds(:,1),old_train_ds(:,2),old_train_ds(:,3),'b.')
plot3(BC_mat(:,1), BC_mat(:,2), BC_mat(:,3), 'ro')
grid on


%% ZEM ZEV DAgger
N = size(BC_mat, 1);
aux = [];

try
    parpool
catch
    %
end
str = strcat('Correcting ', num2str(N), ' states... ');
ppm = ParforProgMon(str, N, 1, 500, 60);

parfor i = 1:N
    BC = BC_mat(i, :);
    
    % Compute trajectory
    tic
    [state, y, time] = GPOPS_1D(BC, tol);
    toc
    
    try
        Mat_ds = [state, y, time];
        Mat_ds = Mat_ds(1,:);
    catch
        skipped_trajectories = skipped_trajectories + 1;
        continue
    end

    aux = [aux; Mat_ds];
    
    ppm.increment()
end


%%
fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)

aux(aux(:,end)>0, :) = [];
ds_augmented = [old_train_ds; aux];
ds_augmented_shuffled = randblock(ds_augmented, [1, size(aux,2)]); % shuffle

fprintf('\nOld training set: %d states\n', size(old_train_ds,1))
fprintf('New states: %d states\n', size(aux,1));
fprintf('Augmented training set: %d states\n', size(ds_augmented,1))

logFileID = fopen(TXT_PATH, 'w');
fprintf(logFileID, 'Old training set: %d states\n', size(old_train_ds,1));
fprintf(logFileID, 'New states: %d states\n', size(aux,1));
fprintf(logFileID, 'Augmented training set: %d states\n', size(ds_augmented,1));
fclose(logFileID);

dlmwrite(SAVE_PATH, ds_augmented_shuffled, 'delimiter', ',', 'precision', 8)

