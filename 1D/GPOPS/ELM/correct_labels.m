clear
close all
clc
rng('shuffle')

dagger_it = 1;
tol = 1e-2;

BC2CORRECT_PATH = strcat('dagger/predictions/bc2correct_', num2str(dagger_it), '.csv');
CORRECTIONS_PATH = strcat('dagger/predictions/corrections_', num2str(dagger_it), '.csv');
TRAIN_PATH = strcat('dagger/train_ds/ds_train_', num2str(dagger_it), '.csv'); 
TEMP_PATH = 'aux_temp.csv';
TEMP_PATH2 = 'aux_temp2.csv';

BC_mat = csvread(BC2CORRECT_PATH);
BC_mat = sortrows(BC_mat, 2, 'ascend'); % correct first the states with low altitude
old_train_ds = csvread(TRAIN_PATH);

%% plot states to correct
figure()
hold on
plot3(old_train_ds(:,1),old_train_ds(:,2),old_train_ds(:,3),'b.')
plot3(BC_mat(:,2), BC_mat(:,3), BC_mat(:,4), 'r.')
grid on
xlabel('Position [m]')
ylabel('Velocity [m/s]')
zlabel('Mass [kg]')


%% correct labels
N = size(BC_mat, 1);
aux = [];
aux2 = []; % to keep skipped trajectories
starting_idx = 1;
skipped_trajectories = 0;


try
    aux = csvread(TEMP_PATH);
    starting_idx = size(aux,1) + 1;
catch
    %
end

str = ['Generating dataset ...', num2str(starting_idx), '/', num2str(N)];
h = waitbar(0, str);
for i = starting_idx:N
    BC = BC_mat(i, :);
    disp(BC)
    
    % Compute trajectory
    tic
    [state, y, time] = GPOPS_1D(BC(2:end), tol);    
    toc
    
    try
        Mat_ds = [state, y, time];
        Mat_ds = [BC(1), Mat_ds(1, :)];
    catch
        skipped_trajectories = skipped_trajectories + 1;
        aux2 = [aux2; BC(1)];
        continue
    end
    
    aux = [aux; Mat_ds];  
    
    dlmwrite(TEMP_PATH, aux, 'delimiter', ',', 'precision', 8)
    dlmwrite(TEMP_PATH2, aux2, 'delimiter', ',', 'precision', 8)
 
    str = ['Generating dataset ... ', num2str(i),'/', num2str(N)];
    waitbar(i/N, h, str)
end
close(h)

%%

fprintf('\nSkipped trajectories: %d\n', skipped_trajectories)

if ~isempty(aux2) % if aux2 is non empty vector
    aux2 = [aux2, zeros(size(aux2,1), size(aux,2)-1)];
    aux = [aux; aux2];
end

aux = sortrows(aux, 1);
aux = aux(:, 2:end); % remove first column

dlmwrite(CORRECTIONS_PATH, aux, 'delimiter', ',', 'precision', 8)



% 'delete(myCluster.Jobs)' to remove all jobs created with profile local.
% To create 'myCluster' use 'myCluster = parcluster('local')'