function dxdt = Eom_ZEM_ZEV(t, X, y_pred)
h = X(1);
vh = X(2);

g0 = 1.622;

hdot = vh;
vhdot = -g0 + y_pred;

dxdt = [hdot; vhdot];
end