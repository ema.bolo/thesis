function ZEM_ZEV_CollectStatesToCorrect(aux)

temp = load(aux.PRED_TRAJ);
predictedTrajectories = temp.predictedTrajectories;

bc2correct = [];
k = 0;

for i = 1:length(predictedTrajectories)
    
    predTraj = predictedTrajectories{i};
    L = size(predTraj, 1);
    bc2correct_vec = 1:aux.d_bc:L-1;
    
    for j = bc2correct_vec
        aux_vec = [k, predTraj(j, 1:3)];
        bc2correct = [bc2correct; aux_vec];
        k = k+1;        
    end
end

dlmwrite(aux.BC2CORRECT, bc2correct, 'delimiter', ',', 'precision', 8)
fprintf('\nStates to correct: %d\n', length(bc2correct))

end