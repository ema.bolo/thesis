function ZEM_ZEV_CollectBCForDagger(aux, show_plot)

% load train dataset
train_ds = csvread(aux.TRAIN_DS);
X_train = train_ds(:,1:2);

% load states corrected
corrections = csvread(aux.CORRECTIONS);
y_correct = corrections(:,4);

% load predicted trajectory
predictedTrajectories = load(aux.PRED_TRAJ);
predictedTrajectories = predictedTrajectories.predictedTrajectories;

% initialize variables
bc4dagger = [];
mse_vec = [];
j = 1;

for idx = 1:length(predictedTrajectories)
    predTraj = predictedTrajectories{idx};
    bc_vec = 1 : aux.d_bc : length(predTraj)-1;
    bc4dagger_temp = [];
    
    
    figure
    subplot(211)
    hold on
    grid on

    for i = bc_vec
        time = predTraj(i,5);
        y_pred = predTraj(i,4);
        y_opt = y_correct(j);
        
        try
            plot(time, y_pred, 'r.')
            plot(time, y_opt, 'b.')
            err = abs(y_pred-y_opt);
            mse_vec = [mse_vec; err^2];
            if err > 0.01
                plot(time, y_pred, 'ko')
                bc4dagger = [bc4dagger; predTraj(i,1:3)];
                bc4dagger_temp = [bc4dagger_temp; i];
            end
        catch
            fprintf('Point %d not available\n', i)
        end
        
        j = j+1;    
    end
    
    subplot(212)
    grid on
    hold on
    plot(X_train(:,1), X_train(:,2), 'b.')
    plot(predTraj(:,1), predTraj(:,2), 'r.')
    plot(predTraj(bc4dagger_temp,1), predTraj(bc4dagger_temp,2), 'ko')    

end

if show_plot==0
    close all
end

dlmwrite(aux.SAVE_BC, bc4dagger, 'delimiter', ',', 'precision', 8)
fprintf('New states collected for Dagger: %d\n', length(bc4dagger))
end