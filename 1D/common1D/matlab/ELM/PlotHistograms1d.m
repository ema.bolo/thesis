function [D_CM_pos, D_CM_vel] = PlotHistograms1d(aux)

n_bins = 201;

test_ds = load(aux.TEST_DS);
temp = load(aux.PRED_TRAJ);
predictedTrajectories = temp.predictedTrajectories;

predTraj = UnrollCellArray(predictedTrajectories);


%% plot histograms

figure()
subplot(211)
hold on
edges = linspace(round(min(test_ds(:,1))),... 
    round(max(test_ds(:,1))), n_bins);
H_pos_opt = histogram(test_ds(:,1), edges, 'Normalization', 'pdf');
H_pos_pred = histogram(predTraj(:,1), edges, 'Normalization', 'pdf');

subplot(212)
hold on
edges = linspace(round(min(test_ds(:,2))),... 
    round(max(test_ds(:,2))), n_bins);
H_vel_opt = histogram(test_ds(:,2), edges, 'Normalization', 'pdf');
H_vel_pred = histogram(predTraj(:,2), edges, 'Normalization', 'pdf');

saveas(gcf, strcat(aux.FIG_ROOT, 'StateDistribution.png'))



%% compute D_CM

n_pos_opt = H_pos_opt.Values;
n_pos_pred = H_pos_pred.Values;
n_vel_opt = H_vel_opt.Values;
n_vel_pred = H_vel_pred.Values;

n_pos_diff = abs(n_pos_opt - n_pos_pred) .* H_pos_opt.BinWidth;
n_vel_diff = abs(n_vel_opt - n_vel_pred) .* H_vel_opt.BinWidth;

D_CM_pos = sum(n_pos_diff.^2);
D_CM_vel = sum(n_vel_diff.^2);

end