function [state, acc, T] = ZEM_ZEV_1D(BC)

options = odeset('reltol',1e-10,'abstol',1e-20);
par.m0 = BC(3);
par.g = 1.622;
dt = 0.05;

%% set initial conditions
X0 = [0; 0; BC(1); 0; 0; BC(2)];


%%%%%%%%%%%%%%%%%%% ZEM/ZEV control for soft landing %%%%%%%%%%%%%%%%%%%%%%
%%  Find control to final point

rf = [0 , 0 , 0]';
vf = [0 , 0 , 0]';

g = [0 , 0 , -par.g]'; % gravity vector

tInt = 10;
x0new = [X0' , par.m0]; t = 0; tgo = 1; state = x0new;
mass = par.m0; T = 0; acc = []; ControlAction = []; Time = 0;
tgo_vec = [];

while tgo > 5e-2
    x0 = x0new;
    ti = t(end);

    r = x0(1:3)';
    v = x0(4:6)';

    tgo = ComputeTgo(r, v, tgo);
    tgo_vec = [tgo_vec; tgo];

    %% find control (ZEM / ZEV)
    % integrate without control
    par.a = [0; 0; 0];
    tnc = [0, tgo];
    [~,Y] = ode113(@vertical,tnc,x0,options,par);
    x0nc = [Y(end,1),Y(end,2),Y(end,3),Y(end,4),Y(end,5),Y(end,6)]';
    rnc = x0nc(1:3);
    vnc = x0nc(4:6);

    ZEM = rf-rnc;
    ZEV = vf-vnc;

    a = (6/tgo^2)*ZEM-(2/tgo)*ZEV;

    par.a = a;

    controlAction = a'*a*dt;
    ControlAction = [ControlAction ; controlAction];

    acc = [acc; a'];

    %% integrate to next time with control
    t = [0, dt];
    [t, Y] = ode113(@vertical, t, x0, options, par);
    x0new = Y(end,:);

    state = [state ; x0new];
    mass = [mass ; x0new(7)];

    Time = Time+dt;

    T = [T; Time];
end

acc = [acc ; a'];
tgo_vec = [tgo_vec; 0];


% 1D problem
state = [state(:,3), state(:,6), tgo_vec, state(:,7)];
acc = acc(:,3);
end
