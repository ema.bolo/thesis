function [dx] = vertical(~, X, par)

g = [0 ,0, -par.g]';
Isp = 200;
g0 = 9.81;

a = par.a;

% States
x = [X(1) X(2) X(3)]';
xdot = [X(4) X(5) X(6)]';

% thrust
a = [a(1) , a(2) , a(3)]';

% Eom
xddot = g+a;
mdot = -(norm(a)*X(7)) / (Isp*g0);

dx = [xdot ; xddot ; mdot];

end