import numpy as np
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib.mlab import griddata
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from scipy.interpolate import interp1d
import matplotlib.patches as mpatches
import pickle
import six
from keras.models import load_model
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
import math

import util, Eom


##########################################################################################
# REGRESSION CURVE AND CONFUSION MATRIX
##########################################################################################
def PlotRegressionCurve(aux, y_pred, y_opt):
    xvec = np.linspace(min(y_opt), max(y_opt), 100)
    fig = plt.figure()
    plt.plot(y_opt, y_pred, '.')
    plt.plot(xvec, xvec, '--')
    plt.ylabel('Predictions')
    plt.xlabel('Targets')
    plt.title('Regression Curve')
    fig.savefig(aux.FIG_ROOT+'RegressionCurve.png')


def PlotConfusionMatrix(aux, y_true, y_pred):
    """
    This function prints and plots the confusion matrix, both normalized and not normalized.
    """

    title_norm = 'Normalized'
    title = 'Without normalization'
    classes = ['Min', 'Max']
    cmap = plt.cm.Blues

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    cm_norm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    print('Confusion matrix, without normalization')
    print(cm)
    print()
    print("Normalized confusion matrix")
    print(cm_norm)

    fig, (ax1, ax2) = plt.subplots(1,2)
    plt.suptitle('Confusion matrix')

    # Plot confusion matrix not normalized
    im1 = ax1.imshow(cm, interpolation='nearest', cmap=cmap)
    # ax1.figure.colorbar(im1, ax=ax1)
    ax1.set(xticks=np.arange(cm.shape[1]),
        yticks=np.arange(cm.shape[0]),
        xticklabels=classes, yticklabels=classes, title=title,
        ylabel='True label', xlabel='Predicted label')

    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax1.text(j, i, format(cm[i, j], 'd'), ha="center", va="center", \
                color="white" if cm[i, j] > thresh else "black")

    # Plot confusion matrix normalized
    im2 = ax2.imshow(cm, interpolation='nearest', cmap=cmap)
    # ax2.figure.colorbar(im2, ax=ax2)
    ax2.set(xticks=np.arange(cm_norm.shape[1]),
        yticks=np.arange(cm_norm.shape[0]),
        xticklabels=classes, yticklabels=classes, title=title_norm,
        ylabel='True label', xlabel='Predicted label')

    # Loop over data dimensions and create text annotations.
    thresh = cm_norm.max() / 2.
    for i in range(cm_norm.shape[0]):
        for j in range(cm_norm.shape[1]):
            ax2.text(j, i, format(cm_norm[i, j], '.3f'), ha="center", va="center", \
                color="white" if cm_norm[i, j] > thresh else "black")

    fig.tight_layout()
    plt.show()
    fig.savefig(aux.FIG_ROOT+'ConfusionMatrix.png')
    return fig


##########################################################################################
# HISTOGRAMS
##########################################################################################
def PlotHistogram1d(aux):
    font = {'size'   : 18}
    import matplotlib
    matplotlib.rc('font', **font)

    # test trajectories
    opt_df = pd.read_csv(aux.TEST_DS, header=None)
    pos_opt = opt_df.iloc[:, 0].values.astype('float32')
    vel_opt = opt_df.iloc[:, 1].values.astype('float32')

    # predicted trajectories
    pred_df = pd.DataFrame(util.Convert_npy2csv(aux.PRED_TRAJ))
    pos_pred = pred_df.iloc[:, 0].values.astype('float32')
    vel_pred = pred_df.iloc[:, 1].values.astype('float32')

    n_bins = aux.n_bins
    pos_bins = np.linspace(-100, 1500, n_bins+1)
    vel_bins = np.linspace(-50, 10, n_bins+1)
    labels = ['Optimal', 'Predicted']

    ######################################################################################
    fig, (ax1, ax2) = plt.subplots(1,2, figsize=(12, 6))
    plt.suptitle('State distribution: Optimal vs Predicted')

    # n: distribution probability function
    # bins:
    # print (np.sum(n_pos_opt * np.diff(bins_pos_opt))) = 1.0
    n_pos_opt, bins_pos_opt, _ = ax1.hist(pos_opt, bins=pos_bins, density=True)
    n_pos_pred, bins_pos_pred, _ = ax1.hist(pos_pred, bins=pos_bins, density=True, alpha=0.5)
    ax1.legend(labels)
    ax1.set_ylabel('Frequency')
    ax1.set_xlabel('Position [m]')

    n_vel_opt, bins_vel_opt, _ = ax2.hist(vel_opt, bins=vel_bins, density=True)
    n_vel_pred, bins_vel_pred, _ = ax2.hist(vel_pred, bins=vel_bins, density=True, alpha=0.5)
    ax2.legend(labels)
    ax2.set_xlabel('Velocity [m/s]')
    fig.savefig(aux.FIG_ROOT+'StateDistribution.png')

    ######################################################################################
    pos_diff = abs(n_pos_opt - n_pos_pred) * np.diff(bins_pos_opt)
    vel_diff = abs(n_vel_opt - n_vel_pred) * np.diff(bins_vel_opt)

    D_CM_pos = sum(pos_diff**2)
    D_CM_vel = sum(vel_diff**2)

    # fig, (ax1, ax2) = plt.subplots(1,2, figsize=(12, 6))
    # plt.suptitle('State distribution difference')
    #
    # ax1.bar(pos_bins[:-1], height=pos_diff, edgecolor='black', linewidth=1.2,
    # width=(pos_bins[1]-pos_bins[0]), align='edge')
    # ax1.set_ylabel('Frequency')
    # ax1.set_xlabel('Position [m]')
    #
    # ax2.bar(vel_bins[:-1], height=vel_diff, edgecolor='black', linewidth=1.2,
    # width=(vel_bins[1]-vel_bins[0]), align='edge')
    # ax2.set_xlabel('Velocity [m/s]')
    #
    # fig.savefig(aux.FIG_ROOT+'StateDistributionDifference.png')
    # plt.show()
    return [D_CM_pos, D_CM_vel]


def PlotHistogram2d(aux):
    # test trajectories
    opt_df = pd.read_csv(aux.TEST_DS, header=None)
    pos_opt = opt_df.iloc[:, 0].values.astype('float32')
    vel_opt = opt_df.iloc[:, 1].values.astype('float32')

    # predicted trajectories
    pred_df = pd.DataFrame(util.Convert_npy2csv(aux.PRED_TRAJ))
    pos_pred = pred_df.iloc[:, 0].values.astype('float32')
    vel_pred = pred_df.iloc[:, 1].values.astype('float32')

    n_bins = aux.n_bins
    n_bins = 100
    # pos_bins = np.linspace(-5, 1000, n_bins)
    # vel_bins = np.linspace(-50, 5, n_bins)
    # labels = ['Optimal', 'Predicted']

    ######################################################################################
    fig, (ax1, ax2, ax3) = plt.subplots(1,3, figsize=(12, 6))

    n_opt, bins_x_opt, bins_y_opt, _ = ax1.hist2d(pos_opt, vel_opt, bins=n_bins, normed=True, cmap=plt.cm.jet)
    ax1.set_title('Optimal distribution')
    ax1.set_xlabel('Position [m]')
    ax1.set_ylabel('Velocity [m/s]')

    n_pred, bins_x_pred, bins_y_pred, _ = ax2.hist2d(pos_pred, vel_pred, bins=n_bins, normed=True, cmap=plt.cm.jet)
    ax2.set_title('Predicted distribution')
    ax2.set_xlabel('Position [m]')
    ax2.set_ylabel('Velocity [m/s]')

    # print(np.sum(n_opt * np.diff(bins_x_opt) * np.diff(bins_y_opt)))
    # print(np.sum(n_pred * np.diff(bins_x_pred) * np.diff(bins_y_pred)))
    ######################################################################################
    diff_distr = abs(n_opt - n_pred) * np.diff(bins_x_opt) * np.diff(bins_y_opt)
    D_CM = np.sum(diff_distr**2)

    ax3.set_title('Difference of distributions')
    X,Y = np.meshgrid(bins_x_opt, bins_y_opt)
    ax3.pcolormesh(X,Y,diff_distr.T, cmap=plt.cm.jet)
    # plt.hist2d(bins_x_pred, bins_y_pred, bins=n_bins, data=diff_distr, normed=True, cmap=plt.cm.jet)
    ax3.set_xlabel('Position [m]')
    ax3.set_ylabel('Velocity [m/s]')

    fig.savefig(aux.FIG_ROOT+'StateDistribution2d.png')
    return D_CM


##########################################################################################
# DECISION AND SURFACE BOUNDARY
##########################################################################################
def PlotDecisionBoundary(aux):
    if aux.net == 'DNN':
        model = load_model(aux.MODEL)
    elif aux.net == 'ELM':
        model = pickle.load(open(aux.MODEL, 'rb'))
    else:
        print('GPOPS_PlotDecisionBoundary: net not recognized')
        return
    scaler = pickle.load(open(aux.SCALER,'rb'))

    N = 100
    m0 = 1300
    x_vec = np.linspace(0, 1500, N)
    y_vec = np.linspace(-50, 0, N)

    fig = plt.figure()
    plt.title('Decision Boundary')

    for i in x_vec:
        for j in y_vec:
            Xi = np.array([i, j, m0]).reshape(1,3)
            y_pred = model.predict(scaler.transform(Xi))
            if aux.net == 'DNN':
                y_pred = y_pred.argmax(axis=-1)

            if y_pred:
                plt.plot(i, j, '.r')
            else:
                plt.plot(i, j, 'b.')
    plt.xlabel('Position [m]')
    plt.ylabel('Velocity [m/s]')
    plt.show()
    fig.savefig(aux.FIG_ROOT+'DecisionBoundary.png')


def PlotSurfaceBoundary(aux):
    if aux.net == 'DNN':
        model = load_model(aux.MODEL)
        scaler, scaler_y = pickle.load(open(aux.SCALERS,'rb'))
    elif aux.net == 'ELM':
        model = pickle.load(open(aux.MODEL, 'rb'))
        scaler = pickle.load(open(aux.SCALER,'rb'))
    else:
        raise Exception('PlotSurfaceBoundary: net not recognized')

    N = 50
    x_vec = np.linspace(0.5, 1500, N)
    y_vec = np.linspace(-50, -0.5, N)
    X, Y = np.meshgrid(x_vec, y_vec)
    y_pred = np.zeros((N, N))

    for i in range(N):
        for j in range(N):
            tgo = Eom.ComputeTgo([x_vec[i], y_vec[j]], 2)
            Xi = np.c_[x_vec[i], y_vec[j], np.array(tgo)]

            y_pred[i, j] = model.predict(scaler.transform(Xi))
            if aux.net == 'DNN':
                y_pred[i, j] = scaler_y.inverse_transform(y_pred[i, j].reshape(-1,1))
            elif aux.net == 'ELM':
                y_pred[i, j] = y_pred[i, j].flatten()

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Plot the surface.
    surf = ax.plot_surface(X, Y, y_pred, cmap=cm.coolwarm, linewidth=0, antialiased=False)

    # Customize the z axis.
    #ax.set_zlim(-1.01, 1.01)
    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

    # Add a color bar which maps values to colors.
    fig.colorbar(surf, shrink=0.5, aspect=5)

    ax.set_title('Surface Boundary')
    ax.set_xlabel('Position [m]')
    ax.set_ylabel('Velocity [m/s]')
    ax.set_zlabel('Acceleration [m/s^2]')
    plt.show()
    fig.savefig(aux.FIG_ROOT+'SurfaceBoundary.png')


def PlotPredictionError2d(aux):
    if aux.net == 'DNN':
        model = load_model(aux.MODEL)
    elif aux.net == 'ELM':
        model = pickle.load(open(aux.MODEL, 'rb'))
    else:
        raise Exception('Net not recognized')

    scaler = pickle.load(open(aux.SCALER,'rb'))

    # test trajectories
    opt_df = pd.read_csv(aux.CORRECTIONS, header=None)
    x = opt_df.iloc[:, 0].values.astype('float32')
    y = opt_df.iloc[:, 1].values.astype('float32')
    y_opt = opt_df.iloc[:, 4].values.astype('float32')

    N = len(x)
    y_pred = np.zeros((N,))

    for i in range(N):
        Xi = opt_df.iloc[i, :3].values.astype('float32')
        y_pred[i] = model.predict(scaler.transform(Xi.reshape(1,-1)))
        y_pred[i] = y_pred[i].flatten()

    y_err = abs(y_pred - y_opt)

    cmap = plt.get_cmap('jet', 100)
    # cmap.set_under('gray')

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    # cax = ax.scatter(x, y, c=y_err, s=5, cmap=cmap, vmin=0.1, vmax=y_err.max())
    cax = ax.scatter(x, y, y_err, c=y_err, s=5, cmap=cmap, marker='o')
    fig.colorbar(cax)
    ax.set_xlabel('Position [m]')
    ax.set_ylabel('Velocity [m/s]')
    ax.set_zlabel('Error [m/s^2]')
    plt.grid()
    plt.show()
    fig.savefig(aux.FIG_ROOT+'PredictionError2d.png')


def PlotPredictionError3d(aux):
    opt_ds = pd.read_csv(aux.CORRECTIONS, header=None)
    x = opt_ds.iloc[:, 0].values
    y = opt_ds.iloc[:, 1].values

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    hist, xedges, yedges = np.histogram2d(x, y, bins=aux.n_bins, range=[[0, 1500], [-50, 0]])

    # Construct arrays for the anchor positions of the 16 bars.
    xpos, ypos = np.meshgrid(xedges[:-1] + 0.25, yedges[:-1] + 0.25, indexing="ij")
    xpos = xpos.ravel()
    ypos = ypos.ravel()
    zpos = 0

    # Construct arrays with the dimensions for the 16 bars.
    dx = dy = 0.5 * np.ones_like(zpos)
    dz = hist.ravel()

    ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color='b', zsort='average')

    plt.show()


##########################################################################################
# PARTIAL DEPENDENCE PLOT
##########################################################################################
def GPOPS_PartialDependecePlot(aux):
    model = load_model(aux.MODEL)
    scaler = pickle.load(open(aux.SCALER, 'rb'))

    N = 100
    pos0 = (500+900)/2
    vel0 = (-44-34)/2
    mass0 = (1200+1400)/2

    # position dependence
    X1 = np.empty((N, 3))
    X1[:,0] = np.linspace(500,900,N)
    X1[:,1] = vel0
    X1[:,2] = mass0
    y1 = model.predict(scaler.transform(X1))
    y1 = y1.argmax(axis=-1)

    # velocity dependence
    X2 = np.empty((N, 3))
    X2[:,0] = pos0
    X2[:,1] = np.linspace(-44,-34,N)
    X2[:,2] = mass0
    y2 = model.predict(scaler.transform(X2))
    y2 = y2.argmax(axis=-1)

    # mass dependence
    X3 = np.empty((N, 3))
    X3[:,0] = pos0
    X3[:,1] = vel0
    X3[:,2] = np.linspace(1200,1400,N)
    y3 = model.predict(scaler.transform(X3))
    y3 = y3.argmax(axis=-1)
    y3 = (X3[:,2] < 1250)

    fig, (ax1, ax2, ax3) = plt.subplots(ncols=3, sharey=True, figsize=(12,4))
    ax1.plot(X1[:,0], y1,'-o')
    ax1.set_ylabel('Prediction')
    ax1.set_xlabel('Position [m]')

    ax2.plot(X2[:,1], y2,'-o')
    ax2.set_xlabel('Velocity [m/s]')

    ax3.plot(X3[:,2], y3,'-o')
    ax3.set_xlabel('Mass [kg]')

    plt.show()
    fig.savefig(aux.FIG_ROOT+'PartialDepedencePlot_mass_const.png')


def ZEM_ZEV_PartialDependecePlot(aux):
    model = load_model(aux.MODEL)
    scaler, scaler_y = pickle.load(open(aux.SCALERS, 'rb'))

    N = 4
    pos0 = 300
    vel0 = -20

    # position dependence
    X1 = np.empty((N, 3))
    X1[:,0] = np.linspace(100,1000,N)
    X1[:,1] = vel0
    for i in range(N):
        X1[i,2] = Eom.ComputeTgo([X1[i,0], vel0], 1)
    y1 = model.predict(scaler.transform(X1))
    y1 = scaler_y.inverse_transform(y1)

    # velocity dependence
    X2 = np.empty((N, 3))
    X2[:,0] = pos0
    X2[:,1] = np.linspace(-20,10,N)
    for i in range(N):
        X2[i,2] = Eom.ComputeTgo([pos0, X2[i,1]], 1)
    y2 = model.predict(scaler.transform(X2))
    y2 = scaler_y.inverse_transform(y2)

    plt.subplot(211)
    plt.grid()
    plt.plot(X1[:,0], y1,'-.')
    plt.ylabel('acc')
    plt.xlabel('pos')

    plt.subplot(212)
    plt.grid()
    plt.plot(X2[:,1], y2,'-.')
    plt.ylabel('acc')
    plt.xlabel('vel')

    plt.show()


##########################################################################################
# OTHER UTILS
##########################################################################################
def PlotTrainHistory(aux):
    history = pickle.load(open(aux.TRAIN_HISTORY, 'rb'))
    h = history.history
    labels = ['Train', 'Validation']
    epochs = np.arange(0, len(h['lr']), 1)

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(12,6))
    ax1.plot(epochs[10:], h['loss'][10:])
    ax1.plot(epochs[10:], h['val_loss'][10:])
    ax1.set_ylabel('Loss')
    ax1.set_xlabel('Epoch')
    ax1.legend(labels)

    ax2.plot(epochs[10:], h['mean_squared_error'][10:])
    ax2.plot(epochs[10:], h['val_mean_squared_error'][10:])
    ax2.set_ylabel('MSE')
    ax2.set_xlabel('Epoch')
    ax2.legend(labels)

    ax3.plot(epochs, h['lr'])
    ax3.set_ylabel('lr')
    plt.show()
    fig.savefig(aux.FIG_ROOT+'TrainHistory.png')


def PlotTrainValLossELM(aux, loss_train_vec, loss_val_vec, neurons_list):
    # loss_train_vec = np.array(loss_train_vec)
    fig = plt.figure()
    plt.plot(neurons_list, loss_train_vec)
    plt.plot(neurons_list, loss_val_vec)
    plt.legend(['Train loss', 'Validation loss'])
    plt.xlabel('Number of neurons')
    plt.ylabel('Loss')
    plt.show()
    fig.savefig(aux.FIG_ROOT+'TrainValLoss.png')


def render_mpl_table(data, col_width=3.0, row_height=0.825, font_size=15):
    header_color = '#40466e'
    row_colors = ['#f1f1f2', 'w']
    edge_color = 'w'
    bbox = [0, 0, 1, 1]
    header_columns = 0

    size = (np.array(data.shape[::-1]) + np.array([0, 1])) * np.array([col_width, row_height])
    fig, ax = plt.subplots(figsize=size)
    ax.axis('off')

    mpl_table = ax.table(cellText=data.values, bbox=bbox, colLabels=data.columns)

    mpl_table.auto_set_font_size(False)
    mpl_table.set_fontsize(font_size)

    for k, cell in  six.iteritems(mpl_table._cells):
        cell.set_edgecolor(edge_color)
        if k[0] == 0 or k[1] < header_columns:
            cell.set_text_props(weight='bold', color='w')
            cell.set_facecolor(header_color)
        else:
            cell.set_facecolor(row_colors[k[0]%len(row_colors) ])
    return fig, ax
