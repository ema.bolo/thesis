import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from decimal import Decimal
import matplotlib.patches as mpatches
import six
import pickle
from keras.models import load_model
import keras
from sklearn.metrics import confusion_matrix
from scipy.integrate import ode
import scipy.optimize

import util_plot, Eom


##########################################################################################
# AUX
##########################################################################################
class Aux():
    def __init__(self, type, net, iter, d_bc, disturbance=None, start_point=0, end_point=None):
        self.net = net
        self.iter = iter
        self.disturbance = disturbance
        self.start_point = start_point
        self.end_point = end_point
        self.n_bins = 50 # bins for histograms plot
        self.n_steps = 100 # number of states of each test trajectory
        self.dt = 0.1 # delta time for Motne Carlo simulation
        self.SR_criteria = [7.5, 0.2]
        self.d_bc = d_bc

        self.TRAIN_DS = type + '/train_ds/ds_train_' + iter + '.csv'
        self.SAVE_BC = type + '/train_ds/bc4dagger_' + iter + '.csv'
        self.LOGS = type + '/train_ds/tsb_' + iter

        self.TEST_DS = '../test_ds/ds_test_' + iter + '.csv'

        self.PRED_TRAJ = type + '/predictions/predictedTrajectories_' + iter + '.npy'
        self.BC2CORRECT = type + '/predictions/bc2correct_' + iter + '.csv'
        self.CORRECTIONS = type + '/predictions/corrections_' + iter + '.csv'

        if net == 'DNN':
            self.MODEL = type + '/model/DNN_' + iter + '.h5'
        elif net == 'ELM':
            self.MODEL = type + '/model/ELM_' + iter + '.p'
        else:
            raise Exception('Net type not valid.')

        self.SCALER = type + '/model/scaler_' + net + '_' + iter + '.p'
        self.TRAIN_LOG = type + '/model/training_DNN_' + iter + '.csv'
        self.TRAIN_HIST = type + '/model/train_hist_DNN_' + iter + '.p'

        self.TEXT_FILE = type + '/results_' + iter + '.txt'
        self.FIG_ROOT =  type + '/img/' + iter + '_'


##########################################################################################
# Monte Carlo Simulation
##########################################################################################
def ZEM_ZEV_MonteCarloSimulation(aux, overwrite=False):
    try:
        np.load(aux.PRED_TRAJ, allow_pickle=True)
        if not overwrite:
            print('Predicted trajectories found, exit Monte Carlo.')
            return
    except:
        pass
    # =============================================================================
    # Data and Model load
    # =============================================================================
    if aux.net == 'DNN':
        model = load_model(aux.MODEL)
        scaler = pickle.load(open(aux.SCALER,'rb'))
    elif aux.net == 'ELM':
        model = pickle.load(open(aux.MODEL, 'rb'))
        scaler = pickle.load(open(aux.SCALER,'rb'))
    else:
        raise Exception('Network not recognized')

    opt_ds = pd.read_csv(aux.TEST_DS, header=None)
    X_opt = opt_ds.iloc[:, :4].values.astype('float32')      # state
    time_opt = opt_ds.iloc[:, 5].values.astype('float32')    # time

    n_trajectories = int(X_opt.shape[0]/aux.n_steps);
    predictedTrajectories = []

    for i in range(n_trajectories):
        j = i*aux.n_steps
        BC = X_opt[j, :]
        t_max = time_opt[j+aux.n_steps-1]

        predictedTrajectory = ZEM_ZEV_PredictTrajectory(aux, model, scaler, BC, t_max)
        predictedTrajectories.append(predictedTrajectory)
        print("Predicted trajectories: " + str(i+1) + "/" + str(n_trajectories))

    predictedTrajectories = np.array(predictedTrajectories)
    np.save(aux.PRED_TRAJ, predictedTrajectories)

    predictedTrajectories_csv = Convert_npy2csv(aux.PRED_TRAJ)
    np.savetxt(aux.PRED_TRAJ[:-3]+'csv', predictedTrajectories_csv, delimiter=',')


def ZEM_ZEV_PredictTrajectory(aux, model, scaler, BC, t_max):
    # =============================================================================
    # Data and Model load
    # =============================================================================
    landed = False

    # =============================================================================
    # Initial conditons
    # =============================================================================
    # initial condition for EoM: pos0, vel0, m0
    state0 = np.delete(BC, 2)

    # First input: pos0, vel0, tgo0
    X_i = np.delete(BC, 3).reshape(1,-1)
    tgo = BC[2]

    # First prediction
    y_pred = model.predict(scaler.transform(X_i))

    # =============================================================================
    # Online integration
    # =============================================================================
    pred_state = BC.reshape(1, -1) # predicted state
    pred_t = y_pred.reshape(-1, 1) # predicted thrust

    time_int = np.arange(0, t_max+10, aux.dt)
    time_int = time_int.reshape(-1, 1)

    for i in range(len(time_int)-1):
        if aux.disturbance:
            y_pred *= np.random.uniform(0.8,1.2,1).reshape(1,1)

        t0 = time_int[i, 0]
        tf = time_int[i+1, 0]

        x_int = ode(Eom.Eom_ZEM_ZEV)
        x_int.set_integrator('dopri5', rtol=1e-10) #, nsteps=100, first_step=1e-6, max_step=1e-1)
        x_int.set_initial_value(state0.reshape(-1,1), t0)

        while x_int.successful() and x_int.t < (tf):
            x_int.set_f_params(y_pred.flatten())
            x_int.integrate(tf, step=True)

        X_new = x_int.y.flatten()
        tgo = Eom.ComputeTgo(X_new[:2], tgo)

        X_new = np.insert(X_new, 2, tgo.reshape(1,-1)) # pos, vel, tgo, mass

        # Make new prediction
        y_pred = model.predict(scaler.transform(X_new[:-1].reshape(1,-1)))

        pred_state = np.r_[pred_state, X_new.reshape(1,-1)]
        pred_t = np.r_[pred_t, y_pred.reshape(1,1)]

        i = i+1
        if x_int.y[0] < 0:
            landed = True
            break

        if x_int.y[1] > 0:
            break

        state0 = x_int.y # define new initial conditions

    if not landed:
        finalPos = min(pred_state[:,0])
        print("Not landed, lowest altitude reached: " + str(finalPos))

    print(pred_state[-1,:2])
    return np.c_[pred_state, pred_t, time_int[:i+1]]


def GPOPS_MonteCarloSimulation(aux, overwrite=False):
    try:
        np.load(aux.PRED_TRAJ, allow_pickle=True)
        if not overwrite:
            print('Predicted trajectories found, exit Monte Carlo.')
            return
    except:
        pass
    # =============================================================================
    # Data and Model load
    # =============================================================================
    if aux.net == 'DNN':
        model = load_model(aux.MODEL)
    elif aux.net == 'ELM':
        model = pickle.load(open(aux.MODEL, 'rb'))
    scaler = pickle.load(open(aux.SCALER, "rb"))
    opt_ds = pd.read_csv(aux.TEST_DS, header=None)
    opt_ds.columns = ['Position','Velocity','Mass','Thrust','Time']
    X_opt = opt_ds.iloc[:, :3].values.astype('float32')      # state: pos, vel and mass
    time_opt = opt_ds.iloc[:, 4].values.astype('float32')    # time

    n_trajectories = int(X_opt.shape[0]/aux.n_steps);
    predictedTrajectories = []
    finalState_vec = []

    for i in range(n_trajectories):
        j = i*aux.n_steps
        BC = X_opt[j, :]
        t_max = time_opt[j+aux.n_steps-1]

        predictedTrajectory = GPOPS_PredictTrajectory(aux, model, scaler, BC, t_max, aux.dt)
        predictedTrajectories.append(predictedTrajectory)
        finalState_vec.append(predictedTrajectory[-1,:2])
        print("Predicted trajectories: " + str(i+1) + "/" + str(n_trajectories))

    predictedTrajectories = np.array(predictedTrajectories)
    np.save(aux.PRED_TRAJ, predictedTrajectories)

    finalState_vec = np.array(finalState_vec)
    finalState_ave = np.sum(abs(finalState_vec), axis=0) / len(finalState_vec)
    print("\nAverage distance of final state from target:")
    print(finalState_ave)


def GPOPS_PredictTrajectory(aux, model, scaler, BC, t_max, dt):
    # =============================================================================
    # Data and Model load
    # =============================================================================
    n_features = 3
    landed = False

    # First input
    X_i = BC[:n_features].reshape(1, n_features)

    # First prediction
    prediction = model.predict(scaler.transform(X_i))
    if aux.net == 'DNN':
        y_pred = prediction.argmax(axis=-1) # predicted thrust min/max
    elif aux.net == 'ELM':
        y_pred = (prediction.flatten() > 0.5) * 1

    # =============================================================================
    # Initial conditons
    # =============================================================================
    state0 = np.array(BC) # vector of initial conditions

    # =============================================================================
    # Online integration
    # =============================================================================
    pred_state = state0.reshape(1, -1)        # predicted state
    pred_t = y_pred.reshape(-1, 1)            # predicted thrust

    time_int = np.arange(0, t_max+10, dt)
    time_int = time_int.reshape(-1, 1)

    for i in range(len(time_int)-1):
        t0 = time_int[i, 0]
        tf = time_int[i+1, 0]

        x_int = ode(Eom.Eom_GPOPS)
        x_int.set_integrator('dopri5', rtol=1e-10) #, nsteps=100, first_step=1e-6, max_step=1e-1)
        x_int.set_initial_value(state0, t0)

        while x_int.successful() and x_int.t < (tf):
            x_int.set_f_params(y_pred)
            x_int.integrate(tf, step=True)

        x_new = x_int.y.reshape(1, -1)

        # Make new prediction
        prediction = model.predict(scaler.transform(x_new))
        if aux.net == 'DNN':
            y_pred = prediction.argmax(axis=-1)
        elif aux.net == 'ELM':
            y_pred = (prediction.flatten() > 0.5) * 1

        pred_state = np.r_[pred_state, x_int.y.reshape(1,-1)]
        pred_t = np.r_[pred_t, y_pred.reshape(1,1)]

        i = i+1
        if x_int.y[0] < 0:
            landed = True
            break

        if x_int.y[1] > 0:
            break

        state0 = x_int.y # define new initial conditions


    if not landed:
        finalPos = min(pred_state[:, 0])
        print("Not landed, lowest altitude reached: " + str(finalPos))

    print(pred_state[-1,:2])
    return np.c_[pred_state, pred_t, time_int[:i+1]]


##########################################################################################
# EVALUATE NETWORK
##########################################################################################
def ZEM_ZEV_EvaluateNetwork(aux):
    if aux.net == 'DNN':
        model = load_model(aux.MODEL)
    elif aux.net == 'ELM':
        model = pickle.load(open(aux.MODEL, 'rb'))
    else:
        raise Exception('Network not recognized')

    scaler = pickle.load(open(aux.SCALER,'rb'))

    test_ds = pd.read_csv(aux.TEST_DS, header=None)
    X_test = test_ds.iloc[:, :3].values
    y_test = test_ds.iloc[:, 4].values

    X_test = scaler.transform(X_test)

    y_pred = model.predict(X_test)
    y_pred = y_pred.flatten()

    # Compute RMSE
    mse = np.mean((y_pred - y_test)**2)
    rmse = np.sqrt(mse)

    util_plot.PlotRegressionCurve(aux, y_pred, y_test)
    print('\nTest RMSE: %.2E' % Decimal(rmse))
    # util_plot.PlotPredictionError2d(aux)
    return rmse


def GPOPS_EvaluateNetwork(aux):
    if aux.net == 'DNN':
        model = load_model(aux.MODEL)
    elif aux.net == 'ELM':
        model = pickle.load(open(aux.MODEL, 'rb'))

    scaler = pickle.load(open(aux.SCALER,'rb'))

    test_ds = pd.read_csv(aux.TEST_DS, header=None)
    X_test = test_ds.iloc[:, :3].values      # state
    y_test = test_ds.iloc[:, 3].values       # target

    idx2remove = [] # remove from test dataset points with wrong label
    for i in range(len(y_test)):
        if y_test[i] > 0 and y_test[i] < 1:
            idx2remove.append(i)
    X_test = np.delete(X_test, idx2remove, axis=0)
    y_test = np.delete(y_test, idx2remove)

    y_test = y_test.astype(int)
    X_test = scaler.transform(X_test)

    y_pred = model.predict(X_test)

    if aux.net == 'DNN':
        y_pred = y_pred.argmax(axis=-1).astype(int)
    elif aux.net == 'ELM':
        y_pred = (y_pred.flatten() > 0.5) * 1

    err = abs(y_pred - y_test)
    acc = np.mean(1-err)
    print('Test accuracy: ' + str(acc))

    # Plot confusion matrix
    util_plot.PlotConfusionMatrix(aux, y_test, y_pred)
    return acc


def GPOPS_ELM_EvaluateNetwork(aux):
    model = pickle.load(open(aux.MODEL,'rb'))
    scaler = pickle.load(open(aux.SCALER,'rb'))

    test_ds = pd.read_csv(aux.TEST_DS, header=None)
    X_test = test_ds.iloc[:, :3].values      # state
    y_test = test_ds.iloc[:, 3].values       # target

    # remove states with thrust between 0 and 1
    idx2remove = []
    for i in range(len(y_test)):
        if y_test[i] > 0 and y_test[i] < 1:
            idx2remove.append(i)
    X_test = np.delete(X_test, idx2remove, axis=0)
    y_test = np.delete(y_test, idx2remove)

    y_test = y_test.astype(int)
    X_test = scaler.transform(X_test)

    y_pred = model.predict(X_test)
    y_pred = y_pred.reshape(-1,1)

    err = abs(y_pred - y_test)
    acc = np.mean(1-err)
    print('acc: ' + str(acc))

    # Plot confusion matrix
    util_plot.PlotConfusionMatrix(aux, y_test, y_pred)

    # util_plot.GPOPS_PlotDecisionBoundary(aux)
    return acc

##########################################################################################
# COLLECT STATES TO CORRECT
##########################################################################################
def ZEM_ZEV_CollectStatesToCorrect(aux):
    predictedTrajectories = np.load(aux.PRED_TRAJ, allow_pickle=True)
    bc2correct = []
    k = 0

    for i in range(len(predictedTrajectories)):
        predTraj = predictedTrajectories[i]
        L = predTraj.shape[0]
        bc2correct_vec = np.arange(int(L*aux.start_point), L, aux.d_bc)

        for j in bc2correct_vec:
            aux_vec = np.c_[np.array([k]).reshape(1,-1), predTraj[j, :2].reshape(1,-1), predTraj[j, 3].reshape(1,1)]
            bc2correct.append(aux_vec)
            k += 1

    bc2correct = np.array(bc2correct).reshape(-1,4)
    np.savetxt(aux.BC2CORRECT, bc2correct, delimiter=',')
    print('States to correct: ' + str(len(bc2correct)))


def GPOPS_CollectStatesToCorrect(aux):
    predictedTrajectories = np.load(aux.PRED_TRAJ, allow_pickle=True)
    bc2correct = []
    k = 0

    for i in range(len(predictedTrajectories)):
        predTraj = predictedTrajectories[i]
        L = predTraj.shape[0]
        bc2correct_vec = np.arange(int(L*aux.start_point), int(L*aux.end_point), aux.d_bc)

        for j in bc2correct_vec:
            aux_temp = np.c_[np.array([k]).reshape(1,1), predTraj[j, :3].reshape(1,-1)]
            bc2correct.append(aux_temp)
            k += 1

    bc2correct = np.array(bc2correct).reshape(-1,4)
    np.savetxt(aux.BC2CORRECT, bc2correct, delimiter=',')
    print('States to correct: ' + str(len(bc2correct)))


##########################################################################################
# COLLECT BC FOR DAGGER
##########################################################################################
def ZEM_ZEV_CollectBCForDAgger(aux, plot=0):
    # load train dataset
    train_ds = pd.read_csv(aux.TRAIN_DS, header=None)
    x = train_ds.iloc[:, :2].values

    # load states corrected
    corrections = pd.read_csv(aux.CORRECTIONS, header=None)
    y_correct = corrections.iloc[:, 4].values

    # load predicted trajectory
    predictedTrajectories = np.load(aux.PRED_TRAJ, allow_pickle=True)

    # initialize variables
    bc4dagger = []
    mse_vec = []
    j = 0 # counter to plot the corrections

    # collect states for dagger
    for idx in range(len(predictedTrajectories)):
        predTraj = predictedTrajectories[idx]
        L = predTraj.shape[0]
        bc_vec = np.arange(int(L*aux.start_point), L, aux.d_bc)

        bc4dagger_temp = []
        ############################################################################
        if plot:
            fig = plt.figure()
            plt.subplot(121)
            plt.grid()

        for i in bc_vec:
            time = predTraj[i,5]
            y_pred = predTraj[i,4]
            y_opt = y_correct[j]

            try:
                if plot:
                    plt.plot(time, y_pred, 'r.')
                    plt.plot(time, y_opt, 'g.')
                err = abs(y_pred - y_opt)
                mse_vec.append(err**2)
                if err > 0.03 and err < 1: # DNN 0.03, 1, ELM 0.01, 0.03
                    if plot:
                        plt.plot(time, y_pred, 'yo', markersize=5, alpha=1)
                    state2append = np.c_[predTraj[i,:2].reshape(1,-1), predTraj[i,3].reshape(1,1)].flatten()
                    bc4dagger.append(state2append) # collect state for dagger
                    bc4dagger_temp.append(i)

            except:
                print('Point ' + str(i) + ' not available\n')
            j += 1


        ############################################################################
        if plot:
            red = mpatches.Patch(color='red', label='Predicted')
            # blue = mpatches.Patch(color='blue', label='Optimal')
            green = mpatches.Patch(color='green', label='Optimal')
            yellow = mpatches.Patch(color='yellow', alpha=1, label='States collected')
            plt.legend(handles=[green, red, yellow])
            plt.xlabel('Time [s]')
            plt.ylabel('Acceleration [m/s^2]')
            plt.subplot(122)
            plt.grid()
            plt.plot(x[:,0], x[:,1], 'b.', alpha=0.3) # plot train dataset
            aux_vec = np.arange(0, len(predTraj), 5)
            plt.plot(predTraj[bc_vec,0], predTraj[bc_vec,1], 'r.', markersize=5) # plot predicted trajectory
            plt.plot(predTraj[bc4dagger_temp,0], predTraj[bc4dagger_temp,1], 'yo', markersize=5, alpha=1)

            red = mpatches.Patch(color='red', label='Trajectory predicted')
            blue = mpatches.Patch(color='blue', label='Train set')
            plt.legend(handles=[blue, red, yellow])
            plt.xlabel('Position [m]')
            plt.ylabel('Velocity [m/s]')
            plt.show()

    print('New states collected for Dagger: ' + str(len(bc4dagger)))
    np.savetxt(aux.SAVE_BC, bc4dagger, delimiter=',')

    mse_vec = np.array(mse_vec)
    mse = mse_vec.mean()
    return mse


def GPOPS_CollectBCForDAgger(aux, plot=0):
    # load train dataset
    train_ds = pd.read_csv(aux.TRAIN_DS, header=None)
    x = train_ds.iloc[:, :2].values

    # load states corrected
    corrections = pd.read_csv(aux.CORRECTIONS, header=None)
    y_correct = corrections.iloc[:, 3].values

    # load predicted trajectory
    predictedTrajectory = np.load(aux.PRED_TRAJ, allow_pickle=True)

    bc4dagger = []
    finalState_vec = []
    acc_vec = []
    j = 0 # counter to plot the corrections
    k = 0 # counter to compute accuracy

    # collect states for dagger
    for idx in range(len(predictedTrajectory)):
        predTraj = predictedTrajectory[idx]
        L = predTraj.shape[0]
        bc_vec = np.arange(int(L*aux.start_point), int(L*aux.end_point), aux.d_bc)

        bc4dagger_temp = []

        ##################################################################################
        fig, (ax1, ax2, ax3) = plt.subplots(3,1)
        for i in bc_vec:
            time = predTraj[i, 4]
            y_pred = predTraj[i, 3]
            y_opt = y_correct[j]

            try:
                if plot:
                    ax1.plot(time, y_pred, 'r.')
                    ax2.plot(time, y_opt, 'b.')
                err = abs(y_pred - y_opt)
                acc_vec.append(1-err)
                k += 1
                if err:
                    ax1.plot(time, y_pred, 'ko', markersize=7, alpha=0.3)
                    bc4dagger.append(predTraj[i,:]) # collect state for dagger
                    bc4dagger_temp.append(i)
            except:
                print('Point ' + str(i) + ' not available\n')
            j += 1

        # red = mpatches.Patch(color='red', label='Predicted')
        # blue = mpatches.Patch(color='blue', label='Optimal')
        # black = mpatches.Patch(color='black', alpha=0.3, label='States collected')
        # plt.legend(handles=[blue, red, black])

        ##################################################################################
        if plot:
            ax1.set_ylabel('Predicted')
            ax2.set_ylabel('Optimal')
            ax3.plot(x[:,0], x[:,1], 'b.', alpha=0.3) # plot train dataset
            ax3.plot(predTraj[:,0],predTraj[:,1],'r.', markersize=3) # plot predicted trajectory
            ax3.plot(predTraj[bc4dagger_temp,0], predTraj[bc4dagger_temp,1], 'ko', markersize=7, alpha=0.8)
            # plot starting and ending point
            ax3.plot(predTraj[bc_vec[0],0],predTraj[bc_vec[0],1],'gx', markersize=7)
            ax3.plot(predTraj[bc_vec[-1],0],predTraj[bc_vec[-1],1],'gx', markersize=7)
            plt.show()
        else:
            plt.close()

    print('New states collected for Dagger: ' + str(len(bc4dagger)))
    np.savetxt(aux.SAVE_BC, bc4dagger, delimiter=',')

    acc = sum(acc_vec)/k
    return acc


##########################################################################################
# GET AND SAVE RESULTS
##########################################################################################
def GetResults(aux, val_loss):
    SR, finalState, D_CM = AnalyzePredictedTrajectories(aux)
    util_plot.PlotHistogram1d(aux)
    D_CM = util_plot.PlotHistogram2d(aux)

    # save results in npy format to plot performance dagger vs supervised
    # val_loss, pos, vel, D_CM
    val_loss = np.array(val_loss).reshape(1,-1)
    SR = np.array(SR).reshape(1,-1)
    finalState = np.array(finalState).reshape(1,-1)
    D_CM = np.array(D_CM).reshape(1,-1)

    results_numpy = np.c_[val_loss, SR, finalState, D_CM].flatten()
    np.save(aux.TEXT_FILE[:-4], results_numpy)
    SaveResults(aux, results_numpy)


def SaveResults(aux, results_numpy):
    f = open(aux.TEXT_FILE, 'w')
    f.write('Dagger iteration: ' + aux.iter)
    f.write('\n')
    f.write('\nTrain set size: ' + str(GetDatasetDimension(aux.TRAIN_DS)) + ' points')
    f.write('\nTest loss:\t\t%.2E' % Decimal(results_numpy[0]))
    f.write('\n')
    f.write('\nMonte Carlo simulation:')
    f.write('\nSuccess rate:\t\t\t%.2f %%' % results_numpy[1])
    f.write('\nAverage final state:\t\t[%.2f, %.2f]' % (results_numpy[2], results_numpy[3]))
    f.write('\nDistance CM:\t\t\t%.2E' % Decimal(results_numpy[4]))
    f.close()


##########################################################################################
# OTHER UTILS
##########################################################################################
def AnalyzePredictedTrajectories(aux):
    predictedTrajectories = np.load(aux.PRED_TRAJ, allow_pickle=True)
    testTrajectories = Convert_csv2npy(aux.TEST_DS, aux.n_steps)

    N = len(predictedTrajectories)
    finalPos_vec = []
    finalVel_vec = []
    # finalState_vec = []
    SR = 0
    D_CM_vec = []
    massOptimality_vec = []

    # Accuracy on y_t and y_theta
    for i in range(N):
        predTraj = predictedTrajectories[i]
        testTraj = testTrajectories[i]

        # Final pos
        finalPos_vec.append(predTraj[-1, 0])

        # Final vel
        if predTraj[-1, 0] < 0.1:
            finalVel_vec.append(predTraj[-1, 1])
        # Final state
        # finalState = predTraj[-1, :2]
        # finalState_vec.append(finalState)

        # Von mises distance
        D_CM = ComputeD_CM(aux, predTraj, testTraj)
        D_CM_vec.append(D_CM)

        # Mass optimality
        m0 = testTraj[0, 3]
        mf_pred = predTraj[-1, 3]
        mf_test = testTraj[-1, 3]
        m_consumed_pred = m0 - mf_pred
        m_consumed_test = m0 - mf_test
        massOptimality = m_consumed_pred / m_consumed_test
        massOptimality_vec.append(massOptimality)

        for j in range(len(predTraj)):
            pos, vel = predTraj[j,:2]
            if abs(pos) < aux.SR_criteria[0] and abs(vel) < aux.SR_criteria[1]:
                SR += 1
                break

    finalPos = np.sum(abs(np.asarray(finalPos_vec))) / len(finalPos_vec)
    finalVel = np.sum(abs(np.asarray(finalVel_vec))) / len(finalVel_vec)

    finalState = np.c_[finalPos.reshape(1,1), finalVel.reshape(1,1)].flatten()
    # finalState = np.sum(abs(finalState_vec), axis=0) / len(finalState_vec)
    print("\nAverage distance of final state from target:")
    print(finalState)

    SR = SR*100/N
    print('\nSuccess rate:')
    print(SR)

    D_CM = np.mean(D_CM_vec)
    print('\nVon Mises Distance:')
    print(D_CM)

    massOptimality = np.mean(massOptimality_vec)
    print('\nMass optimality:')
    print(massOptimality)
    print()
    return SR, finalState, D_CM


def ComputeD_CM(aux, predTraj, optTraj):
    # test trajectories
    pos_opt = optTraj[:, 0]
    vel_opt = optTraj[:, 1]

    # predicted trajectories
    pos_pred = predTraj[:, 0]
    vel_pred = predTraj[:, 1]

    n_bins = aux.n_bins

    ######################################################################################
    n_opt, bins_x_opt, bins_y_opt, _ = plt.hist2d(pos_opt, vel_opt, bins=n_bins, normed=True, cmap=plt.cm.jet)
    n_pred, bins_x_pred, bins_y_pred, _ = plt.hist2d(pos_pred, vel_pred, bins=n_bins, normed=True, cmap=plt.cm.jet)

    # print(np.sum(n_opt * np.diff(bins_x_opt) * np.diff(bins_y_opt)))
    # print(np.sum(n_pred * np.diff(bins_x_pred) * np.diff(bins_y_pred)))
    ######################################################################################
    diff_distr = abs(n_opt - n_pred) * np.diff(bins_x_opt) * np.diff(bins_y_opt)
    D_CM = np.sum(diff_distr**2)
    return D_CM


def Convert_csv2npy(CSV, n_steps):
    Mat_ds = pd.read_csv(CSV, header=None)
    Mat_ds = Mat_ds.iloc[:,:].values.astype('float32')

    n_traj = int(Mat_ds.shape[0]/n_steps)

    optimalTrajectories = np.zeros([n_traj, n_steps, Mat_ds.shape[1]])
    for i in range(n_traj):
        j = i*n_steps
        currentTrajectory = Mat_ds[j:j+n_steps,:]
        optimalTrajectories[i, :, :] = currentTrajectory
    return np.array(optimalTrajectories)


def Convert_npy2csv(DS):
    ds_npy = np.load(DS, allow_pickle=True)
    ds_aux = np.empty((0, ds_npy[0].shape[1]))
    N = len(ds_npy)

    for i in range(N):
        ds_aux = np.append(ds_aux, ds_npy[i], axis=0)
    return ds_aux


def GetDatasetDimension(DS):
    if DS[-3:] == 'csv':
        ds = pd.read_csv(DS, header=None)
    elif DS[-3:] == 'npy':
        ds = pd.DataFrame(Convert_npy2csv(DS))
    else:
        raise Exception('Dataset path not recognized')

    n_traj = int(len(ds.iloc[:,0]))
    return n_traj


def GetCallBacks(aux, FLAGS):
    call_backs = [
        keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=1e-5, patience=15),
        keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.9, patience=5, cooldown=1)]

    if FLAGS.hyper_mode:
        if FLAGS.custom_name != None:
            log_dir = './dagger/logs/train_ds_' + aux.iter + '/' + FLAGS.custom_name
        else:
            log_dir = './dagger/logs/train_ds_' + aux.iter + '/' + GetHyperString(FLAGS)
    else:
        log_dir = aux.LOGS + '/' + GetHyperString(FLAGS)
        call_backs.append(keras.callbacks.ModelCheckpoint(monitor='val_loss', filepath=aux.MODEL, save_best_only=True))
        call_backs.append(keras.callbacks.CSVLogger(aux.TRAIN_LOG, separator=',', append=False))

    call_backs.append(keras.callbacks.TensorBoard(log_dir=log_dir, write_graph=False))
    return call_backs


def GetHyperString(FLAGS):
    layers = FLAGS.layers
    n_hidden = FLAGS.n_hidden
    lr = FLAGS.lr
    batch_size = FLAGS.batch_size
    l2 = FLAGS.l2

    layers = 'layers' + str(layers)
    n_hidden = '_hid' + str(n_hidden)
    lr_aux = str('%.0e' % Decimal(lr))
    lr = '_lr' + lr_aux[0] + 'e-' + lr_aux[-1]
    batch_size = '_batch' + str(batch_size)

    if l2 == 0:
        l2 = '_reg0'
    else:
        l2_aux = str('%.0e' % Decimal(l2))
        l2 = '_reg' + l2_aux[0] + 'e-' + l2_aux[-1]

    hyper_string = layers + n_hidden + lr + batch_size
    return hyper_string


def SelectNeuronsELM(aux):
    pass
