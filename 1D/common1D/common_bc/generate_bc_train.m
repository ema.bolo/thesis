clear; close all; clc

bc_train.z0_min = 1000;
bc_train.z0_max = 1500;
bc_train.vz0_min = -40;
bc_train.vz0_max = -20;
bc_train.m0_min = 1200;
bc_train.m0_max = 1400;

save('bc_train.mat','bc_train')